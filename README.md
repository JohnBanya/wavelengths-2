Wavelengths 2
   Version Number 2.0
   Version Date: 11/20/14

A computer game created entirely in Java.

Note: Work-In-Progress

-
-

![screenshot1.png](https://bitbucket.org/repo/Kz8dX7/images/274048900-screenshot1.png)


-
-

Completed:

- Graphics (Sprite loading, rendering, animation)

- Player movement and controls

- Mouse Interaction

- Keyboard Interaction

- Game Physics

- Collision Detection (Both Player and Particles)

- Level Rendering/Interaction

- Level Loading and Progression

- Level Data

- Six Colors (blue, orange, purple, green, yellow, teal)

- Color Effects (on player/world)


Incomplete:

- Level Designs (Total Number of Levels: 15 / Planned: ???)

- Menu

- Progress Saving/Loading

- Custom Level Creation




*by John Banya*



**Links**

Timelapse: https://www.youtube.com/watch?v=bwwjbJNOqmQ

----------------------------------------------------------------------------------------------------------------------------
**What is Wavelengths?**

Wavelengths is a challenging side-scrolling computer game in which your goal is to solve puzzles by changing the colors of the world around you.

Each individual color has a unique ability to changes the basic properties/physics of the game upon contact with the player.

Levels can be solved by changing the color of tiles to create a method of reaching the (normally inaccessible) end.

-
-

**Controls**

'A'  or  Left Arrow Key: *Move Left*

'D'  or  Right Arrow Key: *Move Right*

'W'  or  Up Arrow Key  or  Space Bar: *Jump*

Number Keys (1-6)  or  Mouse Scroll Wheel: *Change Selected Colors*

Mouse Left Click: *Change tile color*

Mouse Right Click: *Remove tile color*

'R': *Restart Level*

-
-

***Warning: Spoilers***

*Color Properties:*

*Blue: Increased jump (additional boost when connected to other blue tiles)*

*Orange: Increased speed (Resets when changing direction during contact)*

*Purple: Cushion (prevents fall death*

*Green: Reverse gravitational constant (Duration determined by number connected)*

*Yellow: Remove solid property when in contact with player moving above a certain velocity (allows player to pass through normally-solid tiles)*

*Teal: Trampoline (Reflect some y-velocity upon contact)*


----------------------------------------------------------------------------------------------------------------------------
**How to view source code:**

Source -> Wavelengths 2 -> src -> src/res

src: Contains packages of java source files

res: Contains resource files (image files)

*Executable .jar file available in Downloads*