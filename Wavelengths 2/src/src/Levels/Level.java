package src.Levels;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import src.Entities.Entity;
import src.GameData.LevelData;
import src.GameData.PlayerData;
import src.Graphics.AnimatedSprite;
import src.Graphics.Screen;
import src.Graphics.Sprite;
import src.Graphics.SpriteSheet;
import src.Tiles.Tile;
import src.Tiles.Tile_Blank;
import src.Tiles.Tile_Editable;
import src.Tiles.Tile_Solid;
import src.main.Game;

public class Level 
{
	/*
	 		Class to handle all levels present in-game
	 */
	
	private int width_tiles, height_tiles; // Number of tiles in length and width
	private String path;
	
	private int[] tileColors;
	private Tile[] tiles;
	
	// Used to keep track of the number of colored tiles usable per level
	private int totalColored = 0;
	public final int MAX_COLORED;
	
	private LevelData levelData;
	
	private ArrayList<Entity> entities; // Holds the entities currently present in the level
	
	public Level(String path, LevelData levelData)
	{
		this.path = path;
		MAX_COLORED = levelData.getMaxColored();
		this.levelData = levelData;
		entities = new ArrayList<Entity>();
		loadLevel(path);
	}
	
	private void loadLevel(String path)
	{
		try
		{
			// Attempt to load the level image and copy image pixels to int[] tileColors
			long startTime = System.currentTimeMillis(); // Keep track of time needed to load Level
			
			BufferedImage image = ImageIO.read(Game.class.getResource(path));
			width_tiles = image.getWidth();
			height_tiles = image.getHeight();
			tileColors = new int[width_tiles*height_tiles];
			tiles = new Tile[tileColors.length];
			image.getRGB(0, 0, width_tiles, height_tiles, tileColors, 0, width_tiles); // get pixels from image and copy to int[] tileColors
			System.out.println("A Level was loaded successfully in " + (System.currentTimeMillis()-startTime) + " milliseconds");
			
			loadTiles();
		}
		catch(IOException e)
		{
			System.out.println("!!! A Level has failed to load !!!");
			e.printStackTrace();
		}
	}
	
	private void loadTiles()
	{
		// Converts the array of pixels (in int[] tileColors) to an array of Tiles (allowing levels to be loaded with a simple .png image)
		for(int i = 0; i < tileColors.length; i++)
		{
			int colorOfPixel = tileColors[i];
			
			if(colorOfPixel == Tile.COL_EDITABLE) // Add editable tile
				tiles[i] = new Tile_Editable(Sprite.tile_editable);
			else if(colorOfPixel == Tile.COL_SOLID) // Add uneditable tile
				tiles[i] = new Tile_Solid(Sprite.tile_solid);
			else if(colorOfPixel == PlayerData.COLOR_BLUE) // Add blue uneditable tile
				tiles[i] = new Tile_Solid(Sprite.tile_solid, PlayerData.COLOR_BLUE);
			else if(colorOfPixel == PlayerData.COLOR_RED) // Add red uneditable tile
				tiles[i] = new Tile_Solid(Sprite.tile_solid, PlayerData.COLOR_RED);
			else if(colorOfPixel == PlayerData.COLOR_PURPLE) // Add purple uneditable tile
				tiles[i] = new Tile_Solid(Sprite.tile_solid, PlayerData.COLOR_PURPLE);
			else if(colorOfPixel == PlayerData.COLOR_GREEN) // Add green uneditable tile
				tiles[i] = new Tile_Solid(Sprite.tile_solid, PlayerData.COLOR_GREEN);
			else if(colorOfPixel == PlayerData.COLOR_YELLOW) // Add yellow uneditable tile
				tiles[i] = new Tile_Solid(Sprite.tile_solid, PlayerData.COLOR_YELLOW);
			else if(colorOfPixel == PlayerData.COLOR_TEAL) // Add teal uneditable tile
				tiles[i] = new Tile_Solid(Sprite.tile_solid, PlayerData.COLOR_TEAL);
			else if(colorOfPixel == Tile.COL_WINDOW_EMPTY)
				tiles[i] = new Tile_Blank(Sprite.tile_windowEmpty);
			else if(colorOfPixel == Tile.COL_WINDOW_PERSON)
				tiles[i] = new Tile_Blank(Sprite.tile_windowPerson);
			else if(colorOfPixel == Tile.COL_TELEPORTER_BOTTOM)
				tiles[i] = new Tile_Blank(new AnimatedSprite(SpriteSheet.teleporter_sheet_bottom, 0, 0, 6, 4), PlayerData.COLOR_TELEPORT);
			else if(colorOfPixel == Tile.COL_TELEPORTER_MIDDLE)
				tiles[i] = new Tile_Blank(new AnimatedSprite(SpriteSheet.teleporter_sheet_middle, 0, 0, 6, 4), PlayerData.COLOR_TELEPORT);
			else if(colorOfPixel == Tile.COL_TELEPORTER_TOP)
				tiles[i] = new Tile_Blank(new AnimatedSprite(SpriteSheet.teleporter_sheet_top, 0, 0, 6, 4), PlayerData.COLOR_TELEPORT);
			else
				tiles[i] = new Tile_Blank(Sprite.tile_blank);
		}
	}
	
	public void render(Screen screen)
	{
		int xOffset = screen.getXOffset();
		int yOffset = screen.getYOffset();
		
		// Get the four sides of the screen and break the pixels down into tiles (number of tiles that are displayed in each side)
		int x0 = xOffset / 16; // left side
		int x1 = (xOffset + screen.getWidth() + 16) / 16; // right side
		int y0 = yOffset / 16; // top side
		int y1 = (yOffset + screen.getHeight() + 16) / 16; // bottom side
		
		// Loop through visible tiles and render each
		for(int tile_y = y0; tile_y < y1; tile_y++)
		{
			for(int tile_x = x0; tile_x < x1; tile_x++)
			{
				if( (tile_y < height_tiles) && (tile_x < width_tiles) && (tile_y >= 0) && (tile_x >= 0) ) 
				{
					tiles[tile_x + tile_y * width_tiles].render(screen, tile_x, tile_y);
				}
			}
		}
		
		levelData.render(screen);
		
		// Render particles
		for(int i = 0; i < entities.size(); i++)
			entities.get(i).render(screen);
	}
	
	public void tick()
	{
		levelData.tick();
		
		// Tick Entities
		for(int i = 0; i < entities.size(); i++)
		{
			entities.get(i).tick();
			
			if(entities.get(i).isRemoved())
				entities.remove(i);
		}
		
		// Tick Tiles
		for(int i = 0; i < tiles.length; i++)
		{
			tiles[i].tick();
		}
	}
	
	public void addEntity(Entity e)
	{
		entities.add(e);
	}
	
	// --- Return methods ---
	public LevelData getLevelData()
	{
		return levelData;
	}
	
	public Tile getTile(int x, int y)
	{
		// Parameters: The absolute x-y value for the tile, non-scaled
		
		if((x+y*width_tiles) >= tiles.length)
		{
			// x-y input is out of bounds. Returns the last tile in the array.
			return tiles[tiles.length-1];
		}
		else if((x+y*width_tiles) < 0)
		{
			// x-y input is out of bounds. Returns the first tile in the array.
			return tiles[0];
		}
		else
		{
			return tiles[x+y*width_tiles];
		}
	}
	
	public Tile getTile(int index)
	{
		// Parameters: The index of the array to get the tile from
		
		if(index >= tiles.length)
		{
			// index input is out of bounds. Returns the last tile in the array.
			return tiles[tiles.length-1];
		}
		else if(index < 0)
		{
			// index input is out of bounds. Returns the first tile in the array.
			return tiles[0];
		}
		else
		{
			return tiles[index];
		}
	}
	
	public int getTileIndex(int x, int y)
	{
		// Returns the index of the tile from the tiles array based on the given (unscaled) x-y
		return (x+y*width_tiles);
	}
	
	public int getTotalColored()
	{
		return totalColored;
	}
	// --- End Return methods ---
	
	
	// --- Set Methods ---
	public void setTotalColored(int t)
	{
		totalColored = t;
	}
	// --- End Set methods ---
	
}
