package src.Levels;


import src.GameData.*;
import src.Graphics.Screen;

public class LevelLoader 
{
	/*
	 		Loads the level and passes information/variables/objects between the level and the level data
	 */
	
	public static final int TOTAL_LEVELS = 15; // Total number of available levels
	
	private int currentLevelNumber = 0;
	private Level currentLevel;
	
	public LevelLoader()
	{
		loadCurrentLevel();
	}
	
	public void tick()
	{
		currentLevel.tick();
	}
	
	public void render(Screen screen)
	{
		currentLevel.render(screen);
	}
	
	public void loadLevel(int levelNumber)
	{
		// Sets the current level to the level associated with the current level number
		
		if(currentLevelNumber == 0)
		{
			currentLevel = new Level("/res/Levels/Level0.png", new Level_0_Data());
		}
		else if(currentLevelNumber == 1)
		{
			currentLevel = new Level("/res/Levels/Level1.png", new Level_1_Data());
		}
		else if(currentLevelNumber == 2)
		{
			currentLevel = new Level("/res/Levels/Level2.png", new Level_2_Data());
		}
		else if(currentLevelNumber == 3)
		{
			currentLevel = new Level("/res/Levels/Level3.png", new Level_3_Data());
		}
		else if(currentLevelNumber == 4)
		{
			currentLevel = new Level("/res/Levels/Level4.png", new Level_4_Data());
		}
		else if(currentLevelNumber == 5)
		{
			currentLevel = new Level("/res/Levels/Level5.png", new Level_5_Data());
		}
		else if(currentLevelNumber == 6)
		{
			currentLevel = new Level("/res/Levels/Level6.png", new Level_6_Data());
		}
		else if(currentLevelNumber == 7)
		{
			currentLevel = new Level("/res/Levels/Level7.png", new Level_7_Data());
		}
		else if(currentLevelNumber == 8)
		{
			currentLevel = new Level("/res/Levels/Level8.png", new Level_8_Data());
		}
		else if(currentLevelNumber == 9)
		{
			currentLevel = new Level("/res/Levels/Level9.png", new Level_9_Data());
		}
		else if(currentLevelNumber == 10)
		{
			currentLevel = new Level("/res/Levels/Level10.png", new Level_10_Data());
		}
		else if(currentLevelNumber == 11)
		{
			currentLevel = new Level("/res/Levels/Level11.png", new Level_11_Data());
		}
		else if(currentLevelNumber == 12)
		{
			currentLevel = new Level("/res/Levels/Level12.png", new Level_12_Data());
		}
		else if(currentLevelNumber == 13)
		{
			currentLevel = new Level("/res/Levels/Level13.png", new Level_13_Data());
		}
		else if(currentLevelNumber == 14)
		{
			currentLevel = new Level("/res/Levels/Level14.png", new Level_14_Data());
		}
		else if(currentLevelNumber == 15)
		{
			currentLevel = new Level("/res/Levels/Level15.png", new Level_15_Data());
		}
		else
		{
			// Default Level: 1
			currentLevel = new Level("/res/Levels/Level1.png", new Level_1_Data());
		}
	}
	
	public void loadCurrentLevel()
	{
		// Loads the current level
		loadLevel(currentLevelNumber);
	}
	
	public void nextLevel()
	{
		// Increases the level number by one and then loads the current level
		if(currentLevelNumber < TOTAL_LEVELS)
			currentLevelNumber++;
		
		loadCurrentLevel();
	}
	
	
	// --- Return methods ---
	public Level getCurrentLevel()
	{
		return currentLevel;
	}
	
	public int getCurrentLevelNumber()
	{
		return currentLevelNumber;
	}
	
	public int getLevelStartX()
	{
		return currentLevel.getLevelData().getStartX();
	}
	
	public int getLevelStartY()
	{
		return currentLevel.getLevelData().getStartY();
	}
	// --- End Return Methods ---
}
