package src.Graphics;

public class Sprite 
{
	/*
	 		Contains individual sprite data (pixels to represent a given image)
	 */
	
	public final int SIZE;	// Size of the sprite in pixels along a side (typically 16)
	public final int WIDTH, HEIGHT;
	private int[] pixels; // holds the pixels used in the sprite
	
	private SpriteSheet sheet;
	private int sheet_x, sheet_y; // x-y coordinates of sprite on given SpriteSheet
	
	
	// --- Sprites ---
	
	public static Sprite tile_blank = new Sprite(0, 2, SpriteSheet.tile_sheet);
	public static Sprite tile_editable = new Sprite(0, 0, SpriteSheet.tile_sheet);
	public static Sprite tile_solid = new Sprite(1, 0, SpriteSheet.tile_sheet);
	public static Sprite tile_windowEmpty = new Sprite(0, 1, SpriteSheet.tile_sheet);
	public static Sprite tile_windowPerson = new Sprite(1, 1, SpriteSheet.tile_sheet);
	
	public static Sprite gui_colorSelection = new Sprite(0, 0, SpriteSheet.colorSelection_sheet);
	public static Sprite gui_effectBar = new Sprite(SpriteSheet.colorEffectBar_sheet.getPixels(), 48, 8);
	// --- End Sprites ---
	
	
	public Sprite(int sprite_x, int sprite_y, SpriteSheet spriteSheet)
	{
		// sprite_x and sprite_y taken as integers that refer to the ordered number of the sprite (the left (or top) most being '0', and the one after being '1', etc)
		
		sheet_x = sprite_x;
		sheet_y = sprite_y;
		
		sheet = spriteSheet;
		
		SIZE = sheet.getSpriteSize();
		WIDTH = SIZE;
		HEIGHT = SIZE;

		pixels = new int[SIZE*SIZE];
		
		loadSprite();
	}
	
	public Sprite(int[] pixels, int size)
	{
		// Create a sprite directly from pixels without using a spritesheet
		SIZE = size;
		WIDTH = SIZE;
		HEIGHT = SIZE;
		
		this.pixels = pixels;
	}
	
	public Sprite(int[] pixels, int width, int height)
	{
		// Create a sprite directly from pixels without using a spritesheet
		SIZE = width;
		WIDTH = width;
		HEIGHT = height;
		
		this.pixels = pixels;
	}
	
	private void loadSprite()
	{
		int[] sheetPixels = sheet.getPixels();
		
		// Copy the requested pixels from the sprite sheet to int[] pixels
		for(int y = 0; y < SIZE; y++)
		{
			for(int x = 0; x < SIZE; x++)
			{
				pixels[x+y*SIZE] = sheetPixels[(x + sheet_x*SIZE) + (y + sheet_y*SIZE) * sheet.getWidth() ];
			}
		}
		
	}
	
	// --- Return methods ---
	public int[] getPixels()
	{
		return pixels;
	}
	// --- End Return methods ---
	
}
