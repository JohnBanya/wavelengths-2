package src.Graphics;

public class Screen
{
	/*
	 		Contains render methods for systematically formatting pixels to be copied into a displayed image
	 */
	
	private int width, height;
	private int[] pixels;
	private int xOffset, yOffset;
	
	
	public Screen(int width, int height)
	{
		this.width = width;
		this.height = height;
		pixels = new int[width*height]; // total pixels in screen
	}

	public void render(int x, int y, Sprite sprite, boolean ignoreOffsets, boolean swapColors, int[] colorsToReplace, int[] newColors, boolean removeColor, int[] colorsToRemove)
	{
		// Standard Render method
		 
		/*	Parameters: 
		 int x:		x start location to render
		 int y:		y start location to render
		 Sprite sprite:		Sprite to render to screen at given x-y
		 boolean ignoreOffsets:		Ignore screen offsets (stationary on screen at all times)
		 boolean swapColors:		Whether or not to run the method to swap color(s)
		 int[] colorsToReplace:		Int array to hold the colors to replace. Indexes correspond with int[] newColor
		 int[] newColors:		Int array to hold the new colors (that replace the old from int[] colorsToReplace). Indexes correspond with int[] colorsToReplace
		 boolean removeColor:		'true' or 'false' for whether colors are to be removed from a sprite (not rendered at all)
		 int[] colorsToRemove:		The list of colors that should be ignored (not rendered)
		 */
		
		// Adjust x and y for offsets
		if(!ignoreOffsets)
		{
			x -= xOffset;
			y -= yOffset;
		}
		
		int[] spritePixels = sprite.getPixels();
		
		// Do not swap colors if the lengths of colorsToReplace does not equal newColors (they correspond with each other and must be equal)
		if((colorsToReplace.length != newColors.length))
			swapColors = false;
		
		
		for(int y_loop = 0; y_loop < sprite.HEIGHT; y_loop++)
		{
			int y_start = y_loop + y; // Find y start location and begin display from there
			
			for(int x_loop = 0; x_loop < sprite.WIDTH; x_loop++)
			{
				int x_start = x_loop + x; // Find x start location and begin display from there
				
				if(x_start >= 0 && x_start < width && y_start >= 0 && y_start < height) // Avoid out-of-bounds by preventing rendering outside of screen location
				{
					int pixelColor = spritePixels[x_loop+y_loop*sprite.WIDTH];
					
					if(swapColors)
					{
						for(int i = 0; i < colorsToReplace.length; i++)
						{
							if(pixelColor == colorsToReplace[i])
								pixelColor = newColors[i];
						}
					}
					
					if(removeColor)
					{
						// If a color is to be removed, cycle through the list of colors and prevent rendering for each
						for(int i = 0; i < colorsToRemove.length; i++)
						{
							if(pixelColor != colorsToRemove[i])
								pixels[x_start+y_start*width] = pixelColor;
						}
					}
					else
					{
						pixels[x_start+y_start*width] = pixelColor;
					}
				}
			}
			
		}
	}

	public static Sprite replaceColorPercentFromLeft(Sprite sprite, int[] colorsToReplace, int[] newColors, double replacementFromLeft)
	{
		// Replace a color up to a certain % of the total sprite's width from the left
		
		/*
		 	Parameters:
		 		Sprite sprite: 		The sprite to work with
		 		int[] colorsToReplace:		Int array to hold the colors to replace. Indexes correspond with int[] newColor
		 		int[] newColors:		Int array to hold the new colors (that replace the old from int[] colorsToReplace). Indexes correspond with int[] colorsToReplace
		 		double percentReplacementFromLeft:		The % of the total width to replace colors (as a double out of 1.0)
		 */
		
		int[] spritePixels = sprite.getPixels();
		int[] newPixels = new int[spritePixels.length];
	
		for(int y = 0; y < sprite.HEIGHT; y++)
		{
			for(int x = 0; x < sprite.WIDTH; x++)
			{
				int pixelColor = spritePixels[x + y * sprite.WIDTH];
				
				if( (double) x / (sprite.WIDTH-1) <= replacementFromLeft) // If the ratio of the current x and the width is under the left replacement limit, replace the colors
				{
					for(int i = 0; i < colorsToReplace.length; i++)
					{
						if(pixelColor == colorsToReplace[i])
							pixelColor = newColors[i];
					}
				}
			
				newPixels[x + y * sprite.WIDTH] = pixelColor;
			}
		}
		
		return new Sprite(newPixels, sprite.WIDTH, sprite.HEIGHT);
	}
	
	public static AnimatedSprite reverseAnimation(AnimatedSprite sprite)
	{
		// Reverses the animation of an AnimatedSprite
		
		int spriteSize = sprite.getSpriteSize();
		SpriteSheet sheet = new SpriteSheet(sprite.getPixels(), spriteSize, spriteSize * sprite.getAnimationLength(), spriteSize);
		int[] oldPixels = sheet.getPixels();
		
		int[] newPixels = new int[oldPixels.length];
		
		for(int animFrame = sprite.getAnimationLength()-1; animFrame >= 0; animFrame--)
		{
			for(int i = 0; i < spriteSize; i++)
			{
				newPixels[ ((sprite.getAnimationLength()-1)-animFrame)*spriteSize + i ] = oldPixels[ (sprite.getAnimationLength()-1)*spriteSize + i ];
			}
		}
		
		sheet = new SpriteSheet(newPixels, spriteSize, spriteSize * sprite.getAnimationLength(), spriteSize);
		
		return new AnimatedSprite(sheet, 0, 0, sprite.getAnimationLength(), sprite.getAnimationSpeed());
	}
	
	public static Sprite flip(Sprite sprite)
	{
		// Accepts a sprite to flip upside-down and then returns the resulting sprite
		
		int[] spritePixels = sprite.getPixels();
		int[] newPixels = new int[spritePixels.length];
		
		for(int y = 0; y < sprite.HEIGHT; y++)
		{
			for(int x = 0; x < sprite.WIDTH; x++)
			{
				newPixels[x + y * sprite.WIDTH] = spritePixels[((sprite.WIDTH-1)-x) + ((sprite.HEIGHT-1)-y)*sprite.WIDTH];
			}
		}
		
		return new Sprite(newPixels, sprite.WIDTH, sprite.HEIGHT);
	}
	
	public static SpriteSheet flip(SpriteSheet sprite)
	{
		// Accepts a spritesheet to flip upside-down and then returns the resulting spritesheet
		
		int[] spritePixels = sprite.getPixels();
		int[] newPixels = new int[spritePixels.length];
		
		for(int y = 0; y < sprite.getHeight(); y++)
		{
			for(int x = 0; x < sprite.getWidth(); x++)
			{
				newPixels[x + y * sprite.getWidth()] = spritePixels[((sprite.getWidth()-1)-x) + ((sprite.getHeight()-1)-y)*sprite.getWidth()];
			}
		}
		
		return new SpriteSheet(newPixels, sprite.getWidth(), sprite.getHeight(), sprite.getSpriteSize());
	}
	
	public static Sprite mirror(Sprite sprite)
	{
		// Accepts a sprite to mirror (flip left-right) and then returns the resulting sprite
		
		int[] spritePixels = sprite.getPixels();
		int[] newPixels = new int[spritePixels.length];
		
		for(int y = 0; y < sprite.HEIGHT; y++)
		{
			for(int x = 0; x < sprite.WIDTH; x++)
			{
				newPixels[x + y * sprite.WIDTH] = spritePixels[((sprite.WIDTH-1)-x) + y * sprite.WIDTH];
			}
		}
		
		return new Sprite(newPixels, sprite.WIDTH, sprite.HEIGHT);
	}
	
	public static SpriteSheet mirror(SpriteSheet sprite)
	{
		// Accepts a spritesheet to mirror (flip left-right) and then returns the resulting spritesheet
		
		int[] spritePixels = sprite.getPixels();
		int[] newPixels = new int[spritePixels.length];
		
		for(int y = 0; y < sprite.getHeight(); y++)
		{
			for(int x = 0; x < sprite.getWidth(); x++)
			{
				newPixels[x + y * sprite.getWidth()] = spritePixels[((sprite.getWidth()-1)-x) + y * sprite.getWidth()];
			}
		}
		
		return new SpriteSheet(newPixels, sprite.getWidth(), sprite.getHeight(), sprite.getSpriteSize());
	}
	
	public static Sprite getSpriteRectangle(int width, int height, int color)
	{
		// Manually create a rectangle with a given width and height of a given color
		
		int[] newPixels = new int[width * height];
		
		for(int i = 0; i < newPixels.length; i++)
		{
			newPixels[i] = color;
		}
		
		return new Sprite(newPixels, width, height);
	}
	
	// --- Return methods ---
	public int getWidth()
	{
		return width;
	}
	
	public int getHeight()
	{
		return height;
	}
	
	public int[] getPixels()
	{
		return pixels;
	}
	
	public int getXOffset()
	{
		return xOffset;
	}
	
	public int getYOffset()
	{
		return yOffset;
	}
	// --- End Return Methods ---
	
	
	
	// --- Set methods ---
	public void setXOffset(int xOff)
	{
		xOffset = xOff;
	}
	
	public void setYOffset(int yOff)
	{
		yOffset = yOff;
	}
	
	public void clearPixels()
	{
		// Reset all the pixels to black
		for(int i = 0; i < pixels.length; i++)
			pixels[i] = 0;
	}
	// --- End Set methods ---
}
