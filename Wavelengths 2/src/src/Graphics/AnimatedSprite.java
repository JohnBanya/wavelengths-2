package src.Graphics;

public class AnimatedSprite 
{
	/*
	 		Handles the animation of "movie-reel"-like sprites
	 */
	
	private SpriteSheet sheet; // Accepts a spritesheet for loading. Individual sprites are created based on the animation from this spritesheet
	
	private int[] pixels;
	private int animationLength;
	private int sheet_x, sheet_y; // x-y location of the sprite reel to be loaded (unscaled)
	
	private int currentFrame = 0; // Number of the frame. Begins counting at '0'
	
	private int animationSpeed; // Speed at which the animation changes. 60 Ticks per second, so the speed is:		animationSpeed/60 = number of seconds
	private int timer = 0;
	
	private boolean reverse = false;
	
	public AnimatedSprite(SpriteSheet sheet, int x, int y, int animationLength, int animationSpeed)
	{
		this.sheet = sheet;
		sheet_x = x;
		sheet_y = y;
		
		this.animationLength = animationLength;
		this.animationSpeed = animationSpeed;
		
		pixels = new int[sheet.getWidth() * sheet.getHeight()];
		loadSheet();
	}
	
	private void loadSheet()
	{
		// Copy the pixels from the spritesheet to the int[] pixels array
		
		int[] sheetPixels = sheet.getPixels();
		int spriteSize = sheet.getSpriteSize();
		
		for(int y_loop = 0; y_loop < (spriteSize * animationLength); y_loop++)
		{
			for(int x_loop = 0; x_loop < spriteSize; x_loop++)
			{
				pixels[x_loop + y_loop * spriteSize] = sheetPixels[ (x_loop + sheet_x * spriteSize) + (y_loop + sheet_y * spriteSize) * sheet.getWidth()];
			}
		}
	}
	
	public void tick()
	{
		if(!reverse)
		{
			timer++;
			
			if(timer % animationSpeed == 0) // Time the animation to run every certain value (represented by animationSpeed) of ticks
			{
				currentFrame++;
				timer = 0;
				
				if(currentFrame >= animationLength)
				{
					currentFrame = 0;
				}
			}
		}
		else
		{
			if(timer > 0)
				timer--;
			else
				timer = animationSpeed;
			
			if(timer == 0)
			{
				currentFrame--;
				timer = animationSpeed;
				
				if(currentFrame < 0)
				{
					currentFrame = animationLength-1;
				}
			}
		}
	}
	
	public void reset()
	{
		// Reset the sprite
		
		if(!reverse)
		{
			currentFrame = 0;
			timer = 0;
		}
		else
		{
			currentFrame = animationLength-1;
			timer = animationSpeed;
		}
	}
	
	
	public void reverse()
	{
		if(reverse)
			reverse = false;
		else
			reverse = true;
	}

	// --- Return methods ---
	public Sprite getCurrentSprite()
	{
		// Return a certain location of the spritesheet based on the current frame
		
		int spriteSize = sheet.getSpriteSize();
		int[] spritePixels = new int[ spriteSize * spriteSize];
		
		// Copy a square sprite from the spritesheet
		for(int y_loop = 0; y_loop < spriteSize; y_loop++)
		{
			for(int x_loop = 0; x_loop < spriteSize; x_loop++)
			{
				spritePixels[x_loop + y_loop * spriteSize] = pixels[x_loop + (y_loop + currentFrame * spriteSize) * spriteSize];
			}
		}
		
		return new Sprite(spritePixels, spriteSize);
	}
	
	public int getFrameNumber()
	{
		return currentFrame;
	}
	
	public int getAnimationLength()
	{
		return animationLength;
	}
	
	public int getAnimationSpeed()
	{
		return animationSpeed;
	}
	
	public int getSpriteSize()
	{
		return sheet.getSpriteSize();
	}
	
	public int[] getPixels()
	{
		return pixels;
	}
	// --- End Return methods ---
	
	
	// --- Set Methods ---
	public void setCurrentFrame(int frame)
	{
		if(frame < animationLength)
		{
			currentFrame = frame;
		}
	}
	// --- End Set methods ---
}
