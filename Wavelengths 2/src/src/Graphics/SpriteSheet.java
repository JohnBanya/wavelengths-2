package src.Graphics;

import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;

public class SpriteSheet 
{
	/*
			Holds Sprite Sheet data, which typically is comprised of a grouping of sprites
	 */
	
	private String path;
	private int spriteSize;
	private final int WIDTH, HEIGHT;
	private int[] pixels;

	
	// --- SpriteSheets ---
	
	public static SpriteSheet tile_sheet = new SpriteSheet("/res/textures/tiles.png", 32, 48, 16);
	public static SpriteSheet teleporter_sheet_bottom = new SpriteSheet("/res/textures/teleporter_animated_bottom.png", 16, 96, 16);
	public static SpriteSheet teleporter_sheet_middle = new SpriteSheet("/res/textures/teleporter_animated_middle.png", 16, 96, 16);
	public static SpriteSheet teleporter_sheet_top = new SpriteSheet("/res/textures/teleporter_animated_top.png", 16, 96, 16);
	public static SpriteSheet player_sheet = new SpriteSheet("/res/textures/Entity/Player.png", 16, 64, 16);
	public static SpriteSheet font_sheet = new SpriteSheet("/res/textures/font.png", 60, 30, 6);
	public static SpriteSheet colorSelection_sheet = new SpriteSheet("/res/textures/GUI/ColorSelection.png", 12, 12, 12);
	public static SpriteSheet colorEffectBar_sheet = new SpriteSheet("/res/textures/GUI/EffectBar.png", 48, 8, 32);
	
	// --- End SpriteSheets ---
	
	
	public SpriteSheet(String path, int width, int height, int individualSpriteSize)
	{
		this.path = path;
		WIDTH = width;
		HEIGHT = height;
		spriteSize = individualSpriteSize;
		
		loadSpriteSheet();
	}
	
	public SpriteSheet(int[] pixels, int width, int height, int individualSpriteSize)
	{
		WIDTH = width;
		HEIGHT = height;
		spriteSize = individualSpriteSize;
		
		this.pixels = pixels;
	}
	
	private void loadSpriteSheet()
	{
		try
		{
			// Attempt to load the image and copy image pixels to int[] pixels
			long startTime = System.currentTimeMillis(); // Keep track of time needed to load spritesheet
			
			BufferedImage image = ImageIO.read(SpriteSheet.class.getResource(path));
			int w = image.getWidth();
			int h = image.getHeight();
			pixels = new int[w*h];
			image.getRGB(0, 0, w, h, pixels, 0, w); // get pixels from image and copy to int[] pixels
			System.out.println("A Spritesheet was loaded successfully in " + (System.currentTimeMillis()-startTime) + " milliseconds");
		}
		catch(IOException e)
		{
			System.out.println("!!! A Spritesheet has failed to load !!!");
			e.printStackTrace();
		}
	}
	
	// --- Return methods --- 
	public int[] getPixels()
	{
		return pixels;
	}
	
	public int getSpriteSize()
	{
		return spriteSize;
	}
	
	public int getWidth()
	{
		return WIDTH;
	}
	
	public int getHeight()
	{
		return HEIGHT;
	}
	// --- End Return Methods ---
}
