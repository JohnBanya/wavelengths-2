package src.Entities;

import src.Graphics.Screen;
import src.Levels.Level;

public class ParticleSpawner extends Entity
{
	/*
	 	A class to formally generate groups of particles
	 */
	
	private int particleColor;
	private int particleSize;
	private int particleLife;
	private int numberToSpawn; // Number of particles to spawn
	private int spawnSpeed; // How fast to create the particles. A value of '0' will result in the particles being created in a single burst
	
	private double gravityMultiplier = 1.0;
	
	private int timer;
	
	public ParticleSpawner(double x, double y, Level level, int particleColor, int particleSize, int particleLife, int numberToSpawn, int spawnSpeed, double gravityMultiplier)
	{
		super(x, y, level);
		
		this.particleColor = particleColor;
		
		if(particleSize >= 1) // Make sure a valid size is set for the particles
		{
			this.particleSize = particleSize;
		}
		else // Set a default size (1) for the particles
		{
			System.out.println("Particle Spawner generated at (" + x + ", " + y + ") has an invalid size of " + particleSize + "! Setting default: 1");
			this.particleSize = 1;
		}
		
		
		if(particleLife >= 1) // Make sure a valid time is set
		{
			this.particleLife = particleLife;
		}
		else // Set a default lifeTime (1)
		{
			System.out.println("Particle Spawner generated at (" + x + ", " + y + ") has an invalid lifetime of " + particleLife + "! Setting default: 1");
			this.particleLife = 1;
		}
		
		
		if(numberToSpawn >= 1) // Make sure a valid number to spawn is set
		{
			this.numberToSpawn = numberToSpawn;
		}
		else // Set a default number to spawn (1)
		{
			System.out.println("Particle Spawner generated at (" + x + ", " + y + ") has an invalid numberToSpawn of " + numberToSpawn + "! Setting default: 1");
			this.numberToSpawn = 1;
		}
		
		
		if(spawnSpeed >= 0) // Make sure a valid spawnSpeed is set
		{
			this.spawnSpeed = spawnSpeed;
		}
		else // Set a default speed (0)
		{
			System.out.println("Particle Spawner generated at (" + x + ", " + y + ") has an invalid spawnSpeed of " + spawnSpeed + "! Setting default: 0");
			this.spawnSpeed = 0;
		}
		
		this.gravityMultiplier = gravityMultiplier;
	}
	
	public void tick()
	{
		timer++;
		
		if(spawnSpeed == 0) // If spawnSpeed is '0', create the particles in a single burst
		{
			for(int i = 0; i < numberToSpawn; i++)
				level.addEntity(new Particle(x, y, level, particleColor, particleSize, particleLife, gravityMultiplier));
			
			remove(); // remove the particle spawner
		}
		else // Spawn particles over a set period of time
		{
			if(timer >= spawnSpeed && !isRemoved())
			{
				timer = 0;
				level.addEntity(new Particle(x, y, level, particleColor, particleSize, particleLife, gravityMultiplier));
				numberToSpawn--;
				
				if(numberToSpawn <= 0)
					remove();
			}
		}
	}
	
	public void render(Screen screen)
	{
		// Particle Spawners are invisible and do not need to be rendered directly
	}
}
