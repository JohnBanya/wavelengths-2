package src.Entities;

import src.GameData.PlayerData;
import src.Graphics.AnimatedSprite;
import src.Graphics.Screen;
import src.Graphics.Sprite;
import src.Graphics.SpriteSheet;
import src.Levels.Level;
import src.Tiles.Tile;
import src.main.Keyboard;

public class Player extends Entity
{
	/*
	 		Handles player action and stores player data
	 */
	
	// Movement direction enum
	private enum MovementDirection
	{
		RIGHT, LEFT
	}
	
	// Walk speed
	private final double RAW_SPEED = 1.5; // Default speed (used to reset current speed)
	private double speed = RAW_SPEED;  // Current speed (can be modified by objects in-game)
	
	// Player teleportation
	private boolean beginTeleport = false; // 'true' or 'false' for whether to begin a teleport sequence
	private boolean requestTeleport = false; // 'true' or 'false' for whether the player has requested a next-level teleport
	private final int TELEPORT_TIME = 60; // Time (in ticks) needed to pass to complete a teleport
	private final int TELEPORT_FLICKER_TIME = 6; // Amount of time every 10 ticks to stop rendering the player (upon teleport)
	private int teleportTimer = 0; // Counts up when activated until TELEPORT_TIME is reached
	
	// Player modifiers
	public final int SIZE = 16;
	
	// Gravity Physics
	private final double ACCEL_DUE_TO_GRAVITY = 0.08; // Downwards acceleration from gravity
	private double velocity_y = 0.0;
	private boolean jumpAvailable = true; // Whether or not the player is able to jump at a given moment
	private final double JUMP_VELOCITY = -2.60; // Initial velocity for a player jump 
	
	// Color Modifiers
	private final double BLUE_JUMP_MODIFIER = 0.4;
	
	private final double ORANGE_SPEED_MODIFIER = 0.080;
	private final int ORANGE_SPEED_EFFECT_TIME = 180; // Measured in ticks (60 ticks = 1 second)
	private int orange_speed_timer = 0; // Measured in ticks
	private MovementDirection orange_direction = MovementDirection.RIGHT;
	
	private final int GREEN_TIME_ADDITION_PER_TILE = 30; // The effect time of green tiles per tile (Measured in ticks, with 60 ticks = 1 second)
	private int greenEffectTime = 0; // Lasting time of the current green effect
	private int greenTimer = 0; // Keep track of green effect time
	
	private final double YELLOW_MIN_VELOCITY_X = 5.0; // Minimum velocity in the x-axis for passing through yellow tiles
	private final double YELLOW_MIN_VELOCITY_Y = 5.0; // Minimum velocity in the y-axis for passing through yellow tiles
	
	private final double TEAL_MIN_VELOCITY = 3.0; // Minimum y-velocity for the effect of Teal tiles
	private final double TEAL_PERCENT_ENERGY_TRANSFER = 0.80; // The decimal equivalent of the % energy transfer of teal tiles (Note: should always be some energy loss (i.e. less than 1))
	
	// Player Life
	private final double FATAL_VELOCITY = 6.0; // Velocity that results in player death upon contact with ground
	private boolean hasBeenKilled = false; // 'true' or 'false' for whether the player is dead
	public final int DEATH_TIMER = 75; // Number of ticks to hold screen after player death
	
	// Player Modifiers
	private boolean renderPlayer = true; // 'true' or 'false' for whether or not the player should be rendered
	private boolean allowMovement = true; // 'true' or 'false' for whether or not the player should allowed to move
	
	// Player objects
	private Keyboard keyboard;
	
	//Player Graphics
	private AnimatedSprite spriteRight; // Holds animated sprites for movement in the right (positive x) direction
	private AnimatedSprite spriteLeft; // Holds animated sprites for movement in the left (negative x) direction
	
	private AnimatedSprite spriteLeft_reflected, spriteRight_reflected; // Holds reflected animated sprites of spriteLeft and spriteRight
	
	private AnimatedSprite currentSprite; // Holds the current sprite for the player
	private MovementDirection direction = MovementDirection.RIGHT; // Holds the movement direction of the player to determine which sprite to display (left/right)
	
	
	public Player(double x, double y, Level level, Keyboard key) 
	{
		super(x, y, level);
		keyboard = key;
		
		spriteRight = new AnimatedSprite(SpriteSheet.player_sheet, 0, 0, 4, 3);
		spriteLeft = new AnimatedSprite(Screen.mirror(SpriteSheet.player_sheet), 0, 0, 4, 3);
		
		spriteRight_reflected = new AnimatedSprite(Screen.flip(Screen.mirror(SpriteSheet.player_sheet)), 0, 0, 4, 3);
		spriteRight_reflected.reverse();
		spriteLeft_reflected = new AnimatedSprite(Screen.flip(SpriteSheet.player_sheet), 0, 0, 4, 3);
		spriteLeft_reflected.reverse();		
		
		currentSprite = spriteRight;
	}

	private void handleMovement()
	{
		// Handles movement keys and player movement
		
		double x_shift = 0.0;
		double y_shift = 0.0;
		
		x_shift = calculateXShift();
		y_shift = calculateYShift();
		
		// Move in x-axis
		while(x_shift != 0)
		{
			if(Math.abs(x_shift) >= 1) // Handle whole number movement
			{
				if( !tileSolid(roundToNearestInt(x_shift), 0) )
				{
					x += roundToNearestInt(x_shift);
				}
				else if(speed >= YELLOW_MIN_VELOCITY_X) // If the player has sufficient velocity to pass through yellow tiles
				{
					// Check for contact with yellow tiles
					if(getTileAtShift(-1, SIZE/2).getColor() == PlayerData.COLOR_YELLOW) // Left contact
					{
						x += roundToNearestInt(x_shift);
					}
					else if(getTileAtShift(SIZE+1, SIZE/2).getColor() == PlayerData.COLOR_YELLOW) // Right contact
					{
						x += roundToNearestInt(x_shift);
					}
					else if(getTileAtShift(SIZE/2, SIZE/2).getColor() == PlayerData.COLOR_YELLOW) // Center contact
					{
						x += roundToNearestInt(x_shift);
					}
				}
				
				x_shift -= roundToNearestInt(x_shift);
			}
			else // Handle movement for values that are not whole numbers (allows for greater precision of movement)
			{
				if( !tileSolid(roundToNearestInt(x_shift), 0) )
				{
					x += x_shift;
				}
				else if(speed >= YELLOW_MIN_VELOCITY_X) // If the player has sufficient velocity to pass through yellow tiles
				{
					// Check for contact with yellow tiles
					if(getTileAtShift(-1, SIZE/2).getColor() == PlayerData.COLOR_YELLOW) // Left contact
					{
						x += x_shift;
					}
					else if(getTileAtShift(SIZE+1, SIZE/2).getColor() == PlayerData.COLOR_YELLOW) // Right contact
					{
						x += x_shift;
					}
					else if(getTileAtShift(SIZE/2, SIZE/2).getColor() == PlayerData.COLOR_YELLOW) // Center contact
					{
						x += x_shift;
					}
				}
				
				x_shift = 0;
			}
		} // End movement in x-axis
		
		// Move in y-axis
		while(y_shift != 0)
		{
			if(Math.abs(y_shift) >= 1) // Handle whole number movement
			{
				if( !tileSolid(0, roundToNearestInt(y_shift)) )
				{
					y += roundToNearestInt(y_shift);
				}
				else if(Math.abs(velocity_y) >= YELLOW_MIN_VELOCITY_Y) // If the player has sufficient velocity to pass through yellow tiles
				{
					// Check for contact with yellow tiles
					if(getTileAtShift(SIZE/2, SIZE + 1).getColor() == PlayerData.COLOR_YELLOW) // Bottom contact
					{
						y += roundToNearestInt(y_shift);
					}
					else if(getTileAtShift(SIZE/2, -1).getColor() == PlayerData.COLOR_YELLOW) // Top contact
					{
						y += roundToNearestInt(y_shift);
					}
					else if(getTileAtShift(SIZE/2, SIZE/2).getColor() == PlayerData.COLOR_YELLOW) // Center contact
					{
						y += roundToNearestInt(y_shift);
					}
				}
				
				y_shift -= roundToNearestInt(y_shift);
			}
			else // Handle movement for values that are not whole numbers
			{
				if( !tileSolid(0, roundToNearestInt(y_shift)) )
				{
					y += y_shift;
				}
				else if(Math.abs(velocity_y) >= YELLOW_MIN_VELOCITY_Y) // If the player has sufficient velocity to pass through yellow tiles
				{
					// Check for contact with yellow tiles
					if(getTileAtShift(SIZE/2, SIZE + 1).getColor() == PlayerData.COLOR_YELLOW) // Bottom contact
					{
						y += y_shift;
					}
					else if(getTileAtShift(SIZE/2, -1).getColor() == PlayerData.COLOR_YELLOW) // Top contact
					{
						y += y_shift;
					}
					else if(getTileAtShift(SIZE/2, SIZE/2).getColor() == PlayerData.COLOR_YELLOW) // Center contact
					{
						y += y_shift;
					}
				}
				
				y_shift = 0;
			}
		} // End movement in y-axis
		
		// Check red tile contact
		if(tileColorContact(PlayerData.COLOR_RED, 1, 0) || tileColorContact(PlayerData.COLOR_RED, 0, 1) || tileColorContact(PlayerData.COLOR_RED, -1, 0) || tileColorContact(PlayerData.COLOR_RED, 0, -1)) // Check contact with red tiles
		{
			killPlayer();
		}
	}
	
	private boolean tileSolid(double x_shift, double y_shift)
	{
		// returns 'true' or 'false' for whether or not a solid tile is present in the projected motion
		
		boolean solid = false;
		
		// Round the floating point value for x_shift to the next integer value
		if(x_shift > 0)
			x_shift = Math.ceil(x_shift);
		else if(x_shift < 0)
			x_shift = Math.floor(x_shift);
		
		// Round the floating point value for y_shift to the next integer value
		if(y_shift > 0)
			y_shift = Math.ceil(y_shift);
		else if(y_shift < 0)
			y_shift = Math.floor(y_shift);
		
		for(int c = 0; c < 4; c++)
		{
			int x_tile = (int)((x + x_shift) - ((c % 2) * 7 - 11)) / 16;
			int y_tile = (int)((y + y_shift) - ((c / 2) * 15 - 15)) / 16;
			
			if(level.getTile(x_tile, y_tile).getSolid())
			{
				solid = true;
			}
		}
		
		return solid;
	}
	
	private boolean tileColorContact(int color, double x_shift, double y_shift)
	{
		// returns 'true' or 'false' for whether or not a given color tile is present in the projected motion
		
		boolean contactWithColor = false;
		
		// Round the floating point value for x_shift to the next integer value
		if(x_shift > 0)
			x_shift = Math.ceil(x_shift);
		else if(x_shift < 0)
			x_shift = Math.floor(x_shift);
		
		// Round the floating point value for y_shift to the next integer value
		if(y_shift > 0)
			y_shift = Math.ceil(y_shift);
		else if(y_shift < 0)
			y_shift = Math.floor(y_shift);
		
		for(int c = 0; c < 4; c++)
		{
			int x_tile = (int)((x + x_shift) - ((c % 2) * 7 - 11)) / 16;
			int y_tile = (int)((y + y_shift) - ((c / 2) * 15 - 15)) / 16;
			
			if(level.getTile(x_tile, y_tile).getColor() == PlayerData.COLOR_RED)
			{
				contactWithColor = true;
			}
		}
		
		return contactWithColor;
	}
	
	private Tile getTileAtShift(int x_shift, int y_shift)
	{
		// Returns the tile at the given x-y shift
		// Note: Origin (0, 0) starts at top left corner of player
		
		Tile tile;
		
		int xt = 0;
		int yt = 0;
		
		xt = (int)(x + x_shift) / 16;
		yt = (int)(y + y_shift) / 16;
		
		tile = level.getTile(xt, yt);
		
		return tile;
	}
	
	private Tile getTileAtPlayer()
	{
		// Returns the tile at the player's feet (center of player)
		
		Tile tile;
		
		int xt = 0;
		int yt = 0;
		
		if(greenEffectTime == 0)
		{
			xt = (int)(x + SIZE/2) / 16;
			yt = (int)(y + SIZE + 1) / 16;
		}
		else
		{
			xt = (int)(x + SIZE/2) / 16;
			yt = (int)(y - 1) / 16;
		}
		
		tile = level.getTile(xt, yt);
		
		return tile;
	}
	
	private int getTileIndexAtPlayer()
	{
		// Returns the tile's index in the tile array (in the current level) at the center of the mob's feet
		
		int xt = 0;
		int yt = 0;
		
		if(greenEffectTime == 0)
		{
			xt = (int)(x + SIZE/2) / 16;
			yt = (int)(y + SIZE + 1) / 16;
		}
		else
		{
			xt = (int)(x + SIZE/2) / 16;
			yt = (int)(y - 1) / 16;
		}
		
		return level.getTileIndex(xt, yt);
	}
	
	private double calculateXShift()
	{
		// Calculates a new speed value based on world variables
		
		double x_shift = 0.0;
		 
		if(getTileAtPlayer().getColor() == PlayerData.COLOR_ORANGE) // If the tile is orange at the player's feet (center)
		{
			orange_speed_timer = ORANGE_SPEED_EFFECT_TIME;
			
			if(keyboard.left)
			{
				if(direction == MovementDirection.LEFT && orange_direction == MovementDirection.LEFT) // The player is moving left already
				{
					speed += ORANGE_SPEED_MODIFIER;
					orange_direction = MovementDirection.LEFT;
				}
				else // The player has changed directions
				{
					orange_direction = MovementDirection.LEFT;
					direction = MovementDirection.LEFT;
					speed = RAW_SPEED;
				}
				
				x_shift -= speed;
			}
			
			if(keyboard.right)
			{
				if(direction == MovementDirection.RIGHT && orange_direction == MovementDirection.RIGHT) // The player is moving left already
				{
					speed += ORANGE_SPEED_MODIFIER;
					orange_direction = MovementDirection.RIGHT;
				}
				else // The player has changed directions
				{
					orange_direction = MovementDirection.RIGHT;
					direction = MovementDirection.RIGHT;
					speed = RAW_SPEED;
				}
				
				x_shift += speed;
			}
		
		}
		else // No orange tile effects
		{
			if(orange_speed_timer > 0) // Tick the orange timer as long as it is active
			{
				orange_speed_timer--;
				
				if(orange_speed_timer == 0) // if the effect time has ended
				{
					speed = RAW_SPEED; // Reset the speed
				}	
			}
			
			if(keyboard.left)
			{
				x_shift -= speed;
				direction = MovementDirection.LEFT;
			}
			
			if(keyboard.right)
			{
				x_shift += speed;
				direction = MovementDirection.RIGHT;
			}
		}
		
		return x_shift;
	}
	
	private double calculateYShift()
	{
		// Calculates y-axis velocity based on the effects/constants of gravity. Runs 60 times per second (run by tick method)
		// Note: Positive y results in movement in the downward direction (point (0,0) = top left corner of map)
		
		if(greenTimer > 0) // Tick the green tile effect timer as long as it is active
		{
			greenTimer--;
			
			if(greenTimer == 0)
				greenEffectTime = 0;
		}
		
		if(greenEffectTime == 0) // If the player is under the effects of normal gravity
		{
			if(!tileSolid(0, 1)) // Not touching solid ground
			{
				if(tileSolid(0, -1)) // Collision with ceiling
				{
					if(getTileAtShift(SIZE/2, -1).getColor() == PlayerData.COLOR_TEAL && (Math.abs(velocity_y) >= TEAL_MIN_VELOCITY))
						velocity_y *= -TEAL_PERCENT_ENERGY_TRANSFER;
					else
						velocity_y = 0.0;
				}
				
				velocity_y += ACCEL_DUE_TO_GRAVITY; // Add accel due to gravity every tick (to simulate increasing velocity in the downward direction)
			}
			else if((getTileAtShift(SIZE/2, SIZE+1).getColor() == PlayerData.COLOR_YELLOW || getTileAtShift(SIZE/2, SIZE/2).getColor() == PlayerData.COLOR_YELLOW || getTileAtShift(SIZE/2, -1).getColor() == PlayerData.COLOR_YELLOW) && (Math.abs(velocity_y) >= YELLOW_MIN_VELOCITY_Y)) // Check for yellow tile interaction
			{
				velocity_y += ACCEL_DUE_TO_GRAVITY;
			}
			else // touching ground
			{
				if(getTileAtPlayer().getColor() == PlayerData.COLOR_TEAL && (Math.abs(velocity_y) >= TEAL_MIN_VELOCITY)) // If the player makes contact with a teal tile with a certain velocity
				{
					velocity_y *= -TEAL_PERCENT_ENERGY_TRANSFER;
				}
				else if(velocity_y > FATAL_VELOCITY && getTileAtPlayer().getColor() != PlayerData.COLOR_PURPLE) // Player death upon contact with ground that is not purple
				{
					killPlayer();
				}
				else // if the player does not have fatal velocity upon contact with ground
				{
					velocity_y = 0.0;
					
					double jumpModifier = 1.0;
					
					if(jumpAvailable && (keyboard.space || keyboard.up)) // Jump
					{
						jumpModifier = getJumpModifier();
						
						if(getTileAtPlayer().getColor() == PlayerData.COLOR_GREEN) // if the player jumps while touching green
						{
							greenEffectTime = GREEN_TIME_ADDITION_PER_TILE * scanConnected(PlayerData.COLOR_GREEN); // Set the timer
							greenTimer = greenEffectTime;
						}
						
						velocity_y = JUMP_VELOCITY * jumpModifier;
					} // End jump
				}
			}
		}
		else // Player is flipped upside down, reverse all gravitational effects
		{
			if(!tileSolid(0, -1)) // Not touching ground
			{
				if(tileSolid(0, 1)) // Collision with ceiling
				{
					if(getTileAtShift(SIZE/2, SIZE+1).getColor() == PlayerData.COLOR_TEAL && (Math.abs(velocity_y) >= TEAL_MIN_VELOCITY))
						velocity_y *= -TEAL_PERCENT_ENERGY_TRANSFER;
					else
						velocity_y = 0.0;
				}
				
				velocity_y += -ACCEL_DUE_TO_GRAVITY; // Add accel due to gravity every tick (to simulate increasing velocity in the downward direction)
			}
			else if((getTileAtShift(SIZE/2, SIZE+1).getColor() == PlayerData.COLOR_YELLOW || getTileAtShift(SIZE/2, SIZE/2).getColor() == PlayerData.COLOR_YELLOW || getTileAtShift(SIZE/2, -1).getColor() == PlayerData.COLOR_YELLOW) && (Math.abs(velocity_y) >= YELLOW_MIN_VELOCITY_Y)) // Check for yellow tile interaction
			{
				velocity_y += -ACCEL_DUE_TO_GRAVITY;
			}
			else // touching ground
			{
				if(getTileAtPlayer().getColor() == PlayerData.COLOR_TEAL && (Math.abs(velocity_y) >= TEAL_MIN_VELOCITY)) // If the player makes contact with a teal tile with a certain velocity
				{
					velocity_y *= -TEAL_PERCENT_ENERGY_TRANSFER;
				}
				else if(velocity_y < -FATAL_VELOCITY && getTileAtPlayer().getColor() != PlayerData.COLOR_PURPLE) // Player death upon contact with ground that is not purple
				{
					killPlayer();
				}
				else // if the player does not have fatal velocity upon contact with ground
				{
					velocity_y = 0.0;
					
					double jumpModifier = 1.0;
					
					if(jumpAvailable && (keyboard.space || keyboard.up)) // Jump
					{
						jumpModifier = getJumpModifier();
						
						velocity_y = -JUMP_VELOCITY * jumpModifier;
						
						if(getTileAtPlayer().getColor() == PlayerData.COLOR_GREEN) // if the player jumps while touching green
						{
							greenEffectTime = 0; // Reset the timer
							greenTimer = 0;
						}
					} // End jump
				}
			}
		}
		
		return velocity_y;
	}
	
	private double getJumpModifier()
	{
		int numberConnected = scanConnected(PlayerData.COLOR_BLUE); // Number of blue tiles connected in a row
		return (1.0 + (numberConnected * BLUE_JUMP_MODIFIER));
	}
	
	private int scanConnected(int color)
	{
		// Scans the tiles left/right of the player for similar tiles and return the number connected in a row
		
		int numberConnected = 0;
		
		if(getTileAtPlayer().getColor() == color) // Check to see if the color is the correct color at the player's feet
		{
			numberConnected = 1;
			int tileX = getTileIndexAtPlayer(); // Store the index of the tile at the player's feet
			boolean endRight = false, endLeft = false; // 'true' or 'false' for whether a incorrect tile has been reached in the left/right direction
			int offset = 1; // Absolute x offset from the start x (tileX)
			
			while(!endRight) // Scan tiles to the right of the player for # of 'color' tiles
			{
				if(level.getTile(tileX + offset).getColor() == color)
				{
					offset++;
					numberConnected++;
				}
				else
				{
					endRight = true;
				}
			}
			
			offset = 1;
			
			while(!endLeft) // Scan tiles to the left of the player for # of 'color' tiles
			{
				if(level.getTile(tileX - offset).getColor() == color)
				{
					offset++;
					numberConnected++;
				}
				else
				{
					endLeft = true;
				}
			}
		}
		
		return numberConnected;
	}
	
	
	private int roundToNearestInt(double value)
	{
		if(value < 0)
			return -1;
		else
			return 1;
	}
	
	private void handlePlayerSprite()
	{
		// Determine the current sprite of the player (left/right)
		
		if(greenEffectTime == 0) // if the player is not flipped
		{
			if(direction == MovementDirection.LEFT)
			{
				currentSprite = spriteLeft;
			}
			
			if(direction == MovementDirection.RIGHT)
			{
				currentSprite = spriteRight;
			}
	
			
			if(!keyboard.left && !keyboard.right) // No left/right keys are pressed
			{
				currentSprite.reset();
			}
			
			
			if(!tileSolid(0, 1)) // If the player is not touching the ground
			{
					currentSprite.reset();
					currentSprite.setCurrentFrame(1);
			}
		}
		else
		{
			if(direction == MovementDirection.LEFT)
			{
				currentSprite = spriteLeft_reflected;
			}
			
			if(direction == MovementDirection.RIGHT)
			{
				currentSprite = spriteRight_reflected;
			}
			
			if(!keyboard.left && !keyboard.right) // No left/right keys are pressed
			{
				currentSprite.reset();
			}
			
			
			if(!tileSolid(0, -1)) // If the player is not touching the ground
			{
				currentSprite.reset();
				currentSprite.setCurrentFrame(2);
			}
		}
		
		currentSprite.tick();
	}
	
	public void handlePlayerTeleport()
	{
		// Handles teleportation events and detection
		
		if(!beginTeleport && !requestTeleport) // Not currently teleporting
		{
			if(getTileAtShift(SIZE/2, SIZE/2).getColor() == PlayerData.COLOR_TELEPORT) // If the tile at the player has the teleportation color
			{
				beginTeleport = true;
				allowMovement = false;
			}
		}
		
		if(beginTeleport) // When the teleportation sequence has begun
		{
			teleportTimer++;
			
			if(teleportTimer % 10 < TELEPORT_FLICKER_TIME) // Cause flickering of the player when teleporting
			{
				renderPlayer = false;
			}
			else
			{
				renderPlayer = true;
			}
			
			if(teleportTimer >= TELEPORT_TIME-10) // Prevent rendering of player 10 ticks before leaving the level
			{
				renderPlayer = false;
				
				if(teleportTimer == TELEPORT_TIME-10) // Create particles as the player disappears
					level.addEntity(new ParticleSpawner((x+SIZE/2), (y+SIZE/2), level, 0xFF00FFFF, 2, 50, 30, 0, 1.0));
			}
			
			if(teleportTimer == TELEPORT_TIME) // Once the time has been reached to teleport the player, request the next level
			{
				beginTeleport = false;
				requestTeleport = true;
			}
		}
	}
	
	public void tick() 
	{
		if(allowMovement)	// Only allow player movement when the player is not dead
			handleMovement();
		
		handlePlayerSprite();
		handlePlayerTeleport();
	}
	
	public void render(Screen screen) 
	{
		if(renderPlayer)
		{
			screen.render((int) x, (int) y, currentSprite.getCurrentSprite(), false, false, new int[] { 0 }, new int[] { 0 }, true, new int[] { 0xFFFF00DC });
		}
	}
	
	public void reset()
	{
		// Reset variables for player
		velocity_y = 0.0;
		
		speed = RAW_SPEED;
		
		jumpAvailable = true;
		
		hasBeenKilled = false;
		
		orange_speed_timer = 0;
		orange_direction = MovementDirection.RIGHT;
		
		greenTimer = 0;
		greenEffectTime = 0;
		
		allowMovement = true;
		renderPlayer = true;
		
		requestTeleport = false;
		beginTeleport = false;
		teleportTimer = 0;
	}
	
	public void killPlayer()
	{
		if(!hasBeenKilled) // Make sure the player has not already been killed
		{
			hasBeenKilled = true;
			allowMovement = false;
			renderPlayer = false;
			
			velocity_y = 0.0;
			
			if(greenEffectTime == 0) // if the player is not flipped
				level.addEntity(new ParticleSpawner((x+SIZE/2), (y+SIZE/2), level, 0xFFC60000, 2, 30, 140, 0, 1.0));
			else
				level.addEntity(new ParticleSpawner((x+SIZE/2), (y+SIZE/2), level, 0xFFC60000, 2, 30, 140, 0, -1.0));
		}
	}
	
	// --- Return methods ---
	public boolean hasBeenKilled()
	{
		return hasBeenKilled;
	}
	
	public double getVelocityY()
	{
		return velocity_y;
	}
	
	public double getSpeed()
	{
		return speed;
	}
	
	public int getOrangeTimer()
	{
		return orange_speed_timer;
	}
	
	public int getOrangeEffectTime()
	{
		return ORANGE_SPEED_EFFECT_TIME;
	}
	
	public int getGreenTimer()
	{
		return greenTimer;
	}
	
	public int getGreenEffectTime()
	{
		return greenEffectTime;
	}
	
	public boolean hasRequestTeleport()
	{
		return requestTeleport;
	}
	// --- End Return methods ---
}
