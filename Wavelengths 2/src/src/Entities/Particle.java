package src.Entities;

import java.util.Random;

import src.Graphics.Screen;
import src.Graphics.Sprite;
import src.Levels.Level;

public class Particle extends Entity
{
	/*
	 		A single-square graphic of a set color and size
	 */
	
	private Sprite sprite; // Holds the particle's sprite
	
	private int color;
	private int size; // Width and height of the particle in # of pixels
	private int lifeTime;
	
	private double velocity_x, velocity_y; // Velocity of the particle (randomly generated)
	private final double ACCEL_DUE_TO_GRAVITY = 0.08; // Downwards acceleration from gravity
	
	private double gravityMultiplier = 1.0;
	
	private Random rand = new Random();
	
	
	public Particle(double x, double y, Level level, int color, int size, int lifeTime, double gravityMultiplier) 
	{
		super(x, y, level);
		
		this.color = color;
		
		if(size >= 1) // Make sure a valid size is set for the particle
		{
			this.size = size;
		}
		else // Set a default size (1) for the particle
		{
			System.out.println("Particle generated at (" + x + ", " + y + ") has an invalid size of " + size + "! Setting default: 1");
			this.size = 1;
		}
		
		
		if(lifeTime >= 1) // Make sure a valid time is set
		{
			this.lifeTime = lifeTime;
		}
		else // Set a default lifeTime (1)
		{
			System.out.println("Particle generated at (" + x + ", " + y + ") has an invalid lifetime of " + lifeTime + "! Setting default: 1");
			this.lifeTime = 1;
		}
		
		// Create lifetime variation 
		this.lifeTime = rand.nextInt(lifeTime)+lifeTime;
		
		// Create random velocities for the particle (between -1 and 1)
		velocity_x = rand.nextGaussian();
		velocity_y = rand.nextGaussian();
		
		this.gravityMultiplier = gravityMultiplier;
		
		load();
	}
	
	private void load()
	{
		// Create a sprite for the particle based on the size and color
		
		int[] pixels = new int[size*size];
		
		for(int y_loop = 0; y_loop < size; y_loop++)
		{
			for(int x_loop = 0; x_loop < size; x_loop++)
			{
				pixels[x_loop + y_loop * size] = color;
			}
		}
		
		sprite = new Sprite(pixels, size);
	}
	
	public void tick()
	{
		if(tileSolid(velocity_x, 0)) // collision on the x-plane
		{
			velocity_x *= -1; // reverse the velocity upon collision
		}
		
		if(tileSolid(0, velocity_y)) // collision on the y-plane
		{
			velocity_y *= -1; // reverse the velocity upon collision
		}
		
		velocity_y += ACCEL_DUE_TO_GRAVITY * gravityMultiplier; // Create downward acceleration due to gravity
		
		x += velocity_x;
		y += velocity_y;
		
		lifeTime--;
		
		if(lifeTime <= 0)
			remove();
	}
	
	public boolean tileSolid(double x_shift, double y_shift)
	{
		// returns 'true' or 'false' for whether or not a solid tile is present in the projected motion
		
		boolean solid = false;
		
		// Round the floating point value for x_shift to the next integer value
		if(x_shift > 0)
			x_shift = Math.ceil(x_shift);
		else if(x_shift < 0)
			x_shift = Math.floor(x_shift);
		
		// Round the floating point value for y_shift to the next integer value
		if(y_shift > 0)
			y_shift = Math.ceil(y_shift);
		else if(y_shift < 0)
			y_shift = Math.floor(y_shift);
		
		
		for(int c = 0; c < 4; c++)
		{
			int x_tile = (int)((x + x_shift) - ((c % 2) * size)) / 16;
			int y_tile = (int)((y + y_shift) - ((c / 2) * size)) / 16;
			
			if(level.getTile(x_tile, y_tile).getSolid())
			{
				solid = true;
			}
		}
		
		return solid;
	}
	
	public void render(Screen screen)
	{
		screen.render((int)x, (int)y, sprite, false, false, new int[] { 0 }, new int[] { 0 }, false, new int[] { 0 });
	}
}
