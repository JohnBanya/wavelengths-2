package src.Entities;

import src.Graphics.Screen;
import src.Levels.Level;

public abstract class Entity 
{
	/*
	 		Super class for all Entities
	 */
	
	protected double x, y; // x and y coordinate positions
	protected boolean removed = false; // 'true' or 'false' for whether the entity has been deleted and should be removed by the level
	protected Level level; // Used to access level data (current level the entity exists in)
	
	
	public Entity(double x, double y, Level level)
	{
		this.x = x;
		this.y = y;
		this.level = level;
	}
	
	public abstract void tick();
	public abstract void render(Screen screen);
	
	
	
	
	// --- Return Methods ---
	
	public boolean isRemoved()
	{
		return removed;
	}
	
	public double getX()
	{
		return x;
	}
	
	public double getY()
	{
		return y;
	}
	
	// --- End Return methods ---
	
	
	
	// --- Set methods ---
	
	public void remove()
	{
		removed = true;
	}
	
	public void setCoordinates(double x, double y)
	{
		this.x = x;
		this.y = y;
	}
	
	public void setLevel(Level level)
	{
		this.level = level;
	}
	// --- End Set methods ---
}
