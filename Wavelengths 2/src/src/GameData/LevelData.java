package src.GameData;

import src.Entities.Player;
import src.Graphics.Screen;

public abstract class LevelData 
{
	/* 
	 	Super class for Level Data classes
	 */
	
	protected int start_x = 0;
	protected int start_y = 0;
	
	protected int maxColored = 0;
	
	// Allow certain colors to be used in the level
	private boolean allowBlue = false;
	private boolean allowOrange = false;
	private boolean allowPurple = false;
	private boolean allowYellow = false;
	private boolean allowGreen = false;
	private boolean allowTeal = false;
	
	public static final int TEXT_BACKGROUND_COLOR = 0xFFC0C0C0;
	
	
	public LevelData(int x, int y, int maxColored, boolean blue, boolean orange, boolean purple, boolean yellow, boolean green, boolean teal)
	{
		init(x, y, maxColored, blue, orange, purple, yellow, green, teal);
	}

	public abstract void tick();

	public abstract void render(Screen screen);
	
	protected void init(int x, int y, int maxColored, boolean blue, boolean orange, boolean purple, boolean yellow, boolean green, boolean teal)
	{
		this.maxColored = maxColored;
		
		allowBlue = blue;
		allowOrange = orange;
		allowPurple = purple;
		allowYellow = yellow;
		allowGreen = green;
		allowTeal = teal;
		
		start_x = x;
		start_y = y;
	}
	
	
	// --- Return methods ---
	public int getStartX()
	{
		return start_x;
	}
	
	public int getStartY()
	{
		return start_y;
	}
	
	public int getMaxColored()
	{
		return maxColored;
	}
	
	public boolean getAllowBlue()
	{
		return allowBlue;
	}
	
	public boolean getAllowOrange()
	{
		return allowOrange;
	}
	
	public boolean getAllowPurple()
	{
		return allowPurple;
	}
	
	public boolean getAllowYellow()
	{
		return allowYellow;
	}
	
	public boolean getAllowGreen()
	{
		return allowGreen;
	}
	
	public boolean getAllowTeal()
	{
		return allowTeal;
	}
	// --- End Return methods ---
}
