package src.GameData;

import java.util.ArrayList;
import src.Graphics.GameText;
import src.Graphics.Screen;
import src.main.Game;

public class Level_3_Data extends LevelData
{
	/*
	Stores level-specific data for Level 3
	 */
	
	private ArrayList<GameText> text;

	
	public Level_3_Data()
	{
		super(125, 464, 0, false, false, false, false, false, false);
		
		text = new ArrayList<GameText>();
	
		text.add(new GameText("For reasons unknown, objects with a color property", Game.WIDTH/3, Game.HEIGHT/6, 0xFF3100C4, 0, 300, true, true, TEXT_BACKGROUND_COLOR));
		text.add(new GameText("defy our current laws of physics.", Game.WIDTH/3, Game.HEIGHT/5, 0xFF3100C4, 0, 300, true, true, TEXT_BACKGROUND_COLOR));
	}
	
	public void tick()
	{
		// Tick every index of the text ArrayList
		for(int i = 0; i < text.size(); i++)
		{
			text.get(i).tick();
			
			if(text.get(i).isRemoved()) // Remove text that has been completed
				text.remove(i);
		}
	}
	
	public void render(Screen screen)
	{
		// Render every index of the text ArrayList
		for(int i = 0; i < text.size(); i++)
		{
			text.get(i).render(screen);
		}
	}
}
