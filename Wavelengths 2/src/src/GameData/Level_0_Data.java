package src.GameData;

import java.util.ArrayList;
import src.Graphics.GameText;
import src.Graphics.Screen;

public class Level_0_Data extends LevelData
{
	/*
		Stores level-specific data for Level 0 (Test Level)
	*/
	
	private ArrayList<GameText> text;
	
	public Level_0_Data()
	{
		super(1960, 975, 100, true, true, true, true, true, true);
	
		text = new ArrayList<GameText>();
		text.add(new GameText("This is a test level.", 1825, 885, 0xFFFF7F7F, 0, 0, false, false, 0));
		text.add(new GameText("Exit through the teleporter to begin.", 1785, 895, 0xFFFF7F7F, 0, 0, false, false, 0));	
		text.add(new GameText("Exit", 1990, 935, 0xFFDB0000, 0, 0, false, false, 0));
		text.add(new GameText("Test", 1720, 935, 0xFFDB0000, 0, 0, false, false, 0));
	}
	
	public void tick()
	{
		// Tick every index of the text ArrayList
		for(int i = 0; i < text.size(); i++)
		{
			text.get(i).tick();
			
			if(text.get(i).isRemoved()) // Remove text that has been completed
				text.remove(i);
		}
	}
	
	public void render(Screen screen)
	{
		// Render every index of the text ArrayList
		for(int i = 0; i < text.size(); i++)
		{
			text.get(i).render(screen);
		}
	}
}
