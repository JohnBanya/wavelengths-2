package src.GameData;

import java.util.ArrayList;

import src.Graphics.GameText;
import src.Graphics.Screen;
import src.main.Game;

public class Level_1_Data extends LevelData
{
	/*
	 		Stores level-specific data for Level 1
	 */
	
	private ArrayList<GameText> text;
	
	public Level_1_Data()
	{
		super(64, 300, 0, false, false, false, false, false, false);
		
		text = new ArrayList<GameText>();
		text.add(new GameText("(Press  A  or  D to walk)", 100, 290, 0xFFDB0000, 0, 0, false, false, 0));
		text.add(new GameText("Welcome to Operation Wavelengths.", Game.WIDTH/2, Game.HEIGHT/6, 0xFF3100C4, 0, 300, true, true, TEXT_BACKGROUND_COLOR));
		text.add(new GameText("You have been selected from the population to", Game.WIDTH/3, Game.HEIGHT/6, 0xFF3100C4, 300, 600, true, true, TEXT_BACKGROUND_COLOR));
		text.add(new GameText("participate in our exclusive test chambers.", Game.WIDTH/3, Game.HEIGHT/5, 0xFF3100C4, 300, 600, true, true, TEXT_BACKGROUND_COLOR));
		text.add(new GameText("Seems like they let anybody in these days...", Game.WIDTH/3, Game.HEIGHT/5, 0xFF3100C4, 600, 900, true, true, TEXT_BACKGROUND_COLOR));
	}

	public void tick()
	{
		// Tick every index of the text ArrayList
		for(int i = 0; i < text.size(); i++)
		{
			text.get(i).tick();
			
			if(text.get(i).isRemoved()) // Remove text that has been completed
				text.remove(i);
		}
	}
	
	public void render(Screen screen)
	{
		// Render every index of the text ArrayList
		for(int i = 0; i < text.size(); i++)
		{
			text.get(i).render(screen);
		}
	}
	
}
