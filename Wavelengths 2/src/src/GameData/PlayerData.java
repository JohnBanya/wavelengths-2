package src.GameData;

public class PlayerData
{
	/*
	 		Stores the player information
	 */
	
	
	/*
	 	Color Effects:
	 	
	 		Blue: Increased jump (additional boost when connected to other blue tiles)
	 		Orange: Increased speed (stacks with time moving. Cancels out when changing directions)
	 		Purple: Cushion (prevents fall death)
	 		Green: Reverse gravitational constant (for a time determined by the number connected together)
	 		Yellow: Negate solid at certain velocities (allows player to pass through normally-solid tiles)
	 		Teal: Reflect y-velocity upon contact
	 */
	
	private int currentSelectedColor = 0xFFFFFFFF; // Default selected color: White
	private int currentSelectionIndex = 0; // Index of the currently selected color
	
	public static final int COLOR_RED = 0xFFBC0000; // Red color constant. Red will not be an available color to the player
	public static final int COLOR_BLUE = 0xFF0094FF; // Blue color constant
	public static final int COLOR_ORANGE = 0xFFFFB014; // Orange color constant
	public static final int COLOR_PURPLE = 0xFF7B00B5; // Purple color constant
	public static final int COLOR_YELLOW = 0xFFFFE66D; // Yellow color constant
	public static final int COLOR_TEAL = 0xFF00DBDB; // Teal color constant
	public static final int COLOR_GREEN = 0xFF00BC0F; // Green color constant
	
	public static final int COLOR_TELEPORT = 0xFF00FFFF; // Teleportation color constant
	
	// Unlocked-color data
	private boolean unlockedBlue = true;
	private boolean unlockedOrange = true;
	private boolean unlockedPurple = true;
	private boolean unlockedYellow = true;
	private boolean unlockedTeal = true;
	private boolean unlockedGreen = true;
	
	private int currentNumberUnlocked = 0; // Number of colors currently unlocked
	
	
	public void updateCurrentNumberUnlocked()
	{
		// Resets the current number of unlocked colors
		
		currentNumberUnlocked = 0;
		
		if(unlockedBlue)
			currentNumberUnlocked++;
		
		if(unlockedOrange)
			currentNumberUnlocked++;
		
		if(unlockedPurple)
			currentNumberUnlocked++;
		
		if(unlockedYellow)
			currentNumberUnlocked++;
		
		if(unlockedTeal)
			currentNumberUnlocked++;
		
		if(unlockedGreen)
			currentNumberUnlocked++;
	
	}
	
	public void validateIndex()
	{
		// Check to see if the current index is valid
		// Handle "scrolling" of the selection index
		
		updateCurrentNumberUnlocked();
		
		if(currentNumberUnlocked == 0)
			currentSelectionIndex = 0;
		
		if(currentSelectionIndex > currentNumberUnlocked) // Prevents exceeding of currentNumberUnlocked
		{
			currentSelectionIndex = 1;
		}
		
		if(currentSelectionIndex < 1 && currentNumberUnlocked > 0) // If the current selection is less than '1' (no selection) and the current amount of unlocks is not '0'
		{
			currentSelectionIndex = currentNumberUnlocked;
		}
	}
	
	public void updateSelectedColor()
	{
		validateIndex();
		
		if(currentSelectionIndex == 1)
			setBlue();
		if(currentSelectionIndex == 2)
			setOrange();
		if(currentSelectionIndex == 3)
			setPurple();
		if(currentSelectionIndex == 4)
			setYellow();
		if(currentSelectionIndex == 5)
			setTeal();
		if(currentSelectionIndex == 6)
			setGreen();
	}
	
	public void setToLowestAvailableColor()
	{
		// Sets to the lowest available color
		
		if(unlockedBlue)
			currentSelectionIndex = 1;
		else if(unlockedOrange)
			currentSelectionIndex = 2;
		else if(unlockedPurple)
			currentSelectionIndex = 3;
		else if(unlockedYellow)
			currentSelectionIndex = 4;
		else if(unlockedTeal)
			currentSelectionIndex = 5;
		else if(unlockedGreen)
			currentSelectionIndex = 6;
		
		updateSelectedColor();
	}
	
	
	// --- Set methods ---
	public void addToSelectionIndex(int addition)
	{
		// Add an integer amount to the current selection index
		
		currentSelectionIndex += addition;
		
		validateIndex();
	}
	
	public void setSelectionIndex(int s)
	{
		currentSelectionIndex = s;
		
		validateIndex();
	}
	
	public void setBlue()
	{
		if(unlockedBlue)
			currentSelectedColor = COLOR_BLUE;
	}
	
	public void setOrange()
	{
		if(unlockedOrange)
			currentSelectedColor = COLOR_ORANGE;
	}
	
	public void setPurple()
	{
		if(unlockedPurple)
			currentSelectedColor = COLOR_PURPLE;
	}
	
	public void setGreen()
	{
		if(unlockedGreen)
			currentSelectedColor = COLOR_GREEN;
	}
	
	public void setYellow()
	{
		if(unlockedYellow)
			currentSelectedColor = COLOR_YELLOW;
	}
	
	public void setTeal()
	{
		if(unlockedTeal)
			currentSelectedColor = COLOR_TEAL;
	}
	
	
	public void setUnlockedBlue(boolean u)
	{
		unlockedBlue = u;
	}
	
	public void setUnlockedOrange(boolean u)
	{
		unlockedOrange = u;
	}
	
	public void setUnlockedPurple(boolean u)
	{
		unlockedPurple = u;
	}
	
	public void setUnlockedGreen(boolean u)
	{
		unlockedGreen = u;
	}
	
	public void setUnlockedYellow(boolean u)
	{
		unlockedYellow = u;
	}
	
	public void setUnlockedTeal(boolean u)
	{
		unlockedTeal = u;
	}
	// --- End Set methods ---
	
	
	// --- Return methods ---
	public int getCurrentNumberUnlocked()
	{
		return currentNumberUnlocked;
	}
	
	public int getSelectedColor()
	{
		return currentSelectedColor;
	}
	
	public boolean getUnlockedBlue()
	{
		return unlockedBlue;
	}
	
	public boolean getUnlockedOrange()
	{
		return unlockedOrange;
	}
	
	public boolean getUnlockedPurple()
	{
		return unlockedPurple;
	}
	
	public boolean getUnlockedGreen()
	{
		return unlockedGreen;
	}
	
	public boolean getUnlockedYellow()
	{
		return unlockedYellow;
	}
	
	public boolean getUnlockedTeal()
	{
		return unlockedTeal;
	}
	// --- End Return methods ---
}
