package src.GameData;

import java.util.ArrayList;

import src.Graphics.GameText;
import src.Graphics.Screen;

public class Level_10_Data extends LevelData
{
	/*
		Stores level-specific data for Level 10
	 */
	

	private ArrayList<GameText> text;

	public Level_10_Data()
	{
		super(125, 1885, 22, true, true, false, false, false, false);
		text = new ArrayList<GameText>();
	}
	
	public void tick()
	{
		// Tick every index of the text ArrayList
		for(int i = 0; i < text.size(); i++)
		{
			text.get(i).tick();
			
			if(text.get(i).isRemoved()) // Remove text that has been completed
				text.remove(i);
		}
	}
	
	public void render(Screen screen)
	{
		// Render every index of the text ArrayList
		for(int i = 0; i < text.size(); i++)
		{
			text.get(i).render(screen);
		}
	}
}
