package src.GameData;

import java.util.ArrayList;

import src.Graphics.GameText;
import src.Graphics.Screen;
import src.main.Game;

public class Level_2_Data extends LevelData
{
	/*
		Stores level-specific data for Level 2
	 */

	private ArrayList<GameText> text;


	public Level_2_Data()
	{
		super(125, 464, 0, false, false, false, false, false, false);
		
		text = new ArrayList<GameText>();
		text.add(new GameText("(Press SPACE to Jump)", 100, 440, 0xFFDB0000, 0, 0, false, false, 0));
		text.add(new GameText("(Press R to reset a level)", 540, 400, 0xFFDB0000, 0, 0, false, false, 0));
		
		text.add(new GameText("Our latest research involves the use of something", Game.WIDTH/3, Game.HEIGHT/6, 0xFF3100C4, 0, 300, true, true, TEXT_BACKGROUND_COLOR));
		text.add(new GameText("known as colors.", Game.WIDTH/3, Game.HEIGHT/5, 0xFF3100C4, 0, 300, true, true, TEXT_BACKGROUND_COLOR));
		
		text.add(new GameText("I won't attempt to explain them for the safety of", Game.WIDTH/3, Game.HEIGHT/6, 0xFF3100C4, 300, 600, true, true, TEXT_BACKGROUND_COLOR));
		text.add(new GameText("whatever semblance of a brain you currently have.", Game.WIDTH/3, Game.HEIGHT/5, 0xFF3100C4, 300, 600, true, true, TEXT_BACKGROUND_COLOR));
	}
	
	public void tick()
	{
		// Tick every index of the text ArrayList
		for(int i = 0; i < text.size(); i++)
		{
			text.get(i).tick();
			
			if(text.get(i).isRemoved()) // Remove text that has been completed
				text.remove(i);
		}
	}
	
	public void render(Screen screen)
	{
		// Render every index of the text ArrayList
		for(int i = 0; i < text.size(); i++)
		{
			text.get(i).render(screen);
		}
	}
}
