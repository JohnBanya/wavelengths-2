package src.GameData;

import java.util.ArrayList;

import src.Graphics.GameText;
import src.Graphics.Screen;

public class Level_4_Data extends LevelData
{
	/*
	Stores level-specific data for Level 4
	 */

	private ArrayList<GameText> text;

	
	public Level_4_Data()
	{
		super(125, 464, 5, true, false, false, false, false, false);
		
		text = new ArrayList<GameText>();
	
		text.add(new GameText("(Left click on tiles to add color)", 120, 400, 0xFFDB0000, 0, 0, false, false, 0));
		text.add(new GameText("(Right click on tiles to remove color)", 120, 410, 0xFFDB0000, 0, 0, false, false, 0));
	}
	
	public void tick()
	{
		// Tick every index of the text ArrayList
		for(int i = 0; i < text.size(); i++)
		{
			text.get(i).tick();
			
			if(text.get(i).isRemoved()) // Remove text that has been completed
				text.remove(i);
		}
	}
	
	public void render(Screen screen)
	{
		// Render every index of the text ArrayList
		for(int i = 0; i < text.size(); i++)
		{
			text.get(i).render(screen);
		}
	}
}
