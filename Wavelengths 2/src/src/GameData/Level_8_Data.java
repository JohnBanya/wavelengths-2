package src.GameData;

import java.util.ArrayList;

import src.Graphics.GameText;
import src.Graphics.Screen;
import src.main.Game;

public class Level_8_Data extends LevelData
{
	/*
	Stores level-specific data for Level 8
	 */

	private ArrayList<GameText> text;
	
	
	public Level_8_Data()
	{
		super(125, 350, 10, true, true, false, false, false, false);
		
		text = new ArrayList<GameText>();
		text.add(new GameText("You have unlocked the color ", Game.WIDTH/3, Game.HEIGHT/6, 0xFF3100C4, 0, 300, true, true, TEXT_BACKGROUND_COLOR));
		text.add(new GameText("Orange!", Game.WIDTH/3 + text.get(0).getWidth(), Game.HEIGHT/6, 0xFFFF6A00, 0, 300, true, true, TEXT_BACKGROUND_COLOR));
		
		text.add(new GameText("(Use the number keys or the mouse wheel to change colors)", 25, 325, 0xFFDB0000, 0, 0, false, false, 0));
	}
	
	public void tick()
	{
		// Tick every index of the text ArrayList
		for(int i = 0; i < text.size(); i++)
		{
			text.get(i).tick();
			
			if(text.get(i).isRemoved()) // Remove text that has been completed
				text.remove(i);
		}
	}
	
	public void render(Screen screen)
	{
		// Render every index of the text ArrayList
		for(int i = 0; i < text.size(); i++)
		{
			text.get(i).render(screen);
		}
	}
}
