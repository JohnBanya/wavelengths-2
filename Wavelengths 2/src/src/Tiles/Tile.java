package src.Tiles;
import src.Graphics.AnimatedSprite;
import src.Graphics.Screen;
import src.Graphics.Sprite;

public abstract class Tile
{
	/*
	 		Super class for tiles
	 */
	
	protected int tileColor = 0xFFFFFFFF; // Holds the current tile color (used in editable tiles and pre-set solid tiles
	
	protected boolean editable; // Whether or not the tile can be changed
	protected boolean solid;	// Whether or not the player can move through the tile
	protected boolean animated; // Whether or not the tile has an animation
	
	protected Sprite sprite; // Tile sprite
	protected AnimatedSprite animSprite; // Animated Sprite
	public final int SIZE; // Typically 16 as sprites are generally 16x16
	

	// --- Tile Indicator Colors ---
	public static final int COL_EDITABLE = 0xFF808080;
	public static final int COL_SOLID = 0xFF000000;
	public static final int COL_WINDOW_EMPTY = 0xFFFFD800;
	public static final int COL_WINDOW_PERSON = 0xFFE5BF00;
	public static final int COL_TELEPORTER_BOTTOM = 0xFF00E5E5;
	public static final int COL_TELEPORTER_MIDDLE = 0xFF00FFFF;
	public static final int COL_TELEPORTER_TOP = 0xFF00C6C6;
	// --- End Tile Indicator Colors ---
	
	
	public Tile(Sprite sprite)
	{
		// Default color is white ("no color"), no animation
		
		this.sprite = sprite;
		SIZE = sprite.SIZE;
		
		animated = false;
		animSprite = null;
	}
	
	public Tile(Sprite sprite, int defaultColor)
	{
		// No animation
		
		this.sprite = sprite;
		SIZE = sprite.SIZE;
		
		tileColor = defaultColor;
		animated = false;
		animSprite = null;
	}
	
	public Tile(AnimatedSprite animSprite)
	{
		// Animation with default white color
		this.animSprite = animSprite;
		sprite = animSprite.getCurrentSprite();
		
		SIZE = animSprite.getSpriteSize();
		
		animated = true;
	}
	
	public Tile(AnimatedSprite animSprite, int defaultColor)
	{
		// Animation with default white color
		this.animSprite = animSprite;
		sprite = animSprite.getCurrentSprite();
		
		tileColor = defaultColor;
		
		SIZE = animSprite.getSpriteSize();
		
		animated = true;
	}
	
	public abstract void render(Screen screen, int tile_x, int tile_y);
	public abstract void tick();
	
	
	public void setColor(int newColor)
	{
		if(editable)
			tileColor = newColor;
	}
	
	
	// --- Return methods ---
	public boolean getSolid()
	{
		return solid;
	}
	
	public boolean getEditable()
	{
		return editable;
	}
	
	public int getColor()
	{
		return tileColor;
	}
	// --- End return methods ---
	
}
