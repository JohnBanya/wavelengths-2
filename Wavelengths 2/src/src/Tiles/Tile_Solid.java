package src.Tiles;

import src.Graphics.AnimatedSprite;
import src.Graphics.Screen;
import src.Graphics.Sprite;

public class Tile_Solid extends Tile
{

	/*
			Tile that acts as a solid non-color-editable tile
	 */
	protected int tileColor = 0xFFFFFFFF;
	
	protected boolean solid = true;
	protected boolean editable = false;

	int[] colorsToSwap = { 0xFFFF00DC, 0xFFCC00B0 };
	
	
	public Tile_Solid(Sprite sprite)
	{
		super(sprite);
	}
	
	public Tile_Solid(Sprite sprite, int defaultColor)
	{
		super(sprite, defaultColor);
		tileColor = defaultColor;
	}
	
	public Tile_Solid(AnimatedSprite animSprite)
	{
		super(animSprite);
	}
	
	public Tile_Solid(AnimatedSprite animSprite, int defaultColor)
	{
		super(animSprite, defaultColor);
		tileColor = defaultColor;
	}
	
	
	public void render(Screen screen, int tile_x, int tile_y)
	{
		// Parameters of x/y are assumed to be in compressed format (tile order number, such as tile number '2' instead of x value 32)
		int x = tile_x * SIZE;
		int y = tile_y * SIZE;
		
		if(!animated)
			screen.render(x, y, sprite, false, true, colorsToSwap, new int[] { tileColor, 0xFFCC00B0 }, false, new int[] { 0 });
		else
			screen.render(x, y, animSprite.getCurrentSprite(), false, true, colorsToSwap, new int[] { tileColor, 0xFFCC00B0 }, true, new int[] { 0xFFCC00B0 });
	}
	
	public void tick()
	{
		if(animated)
			animSprite.tick();
	}
	
	public void setColor(int newColor)
	{
		if(editable)
			tileColor = newColor;
	}
	
	// --- Return methods ---
	public boolean getSolid()
	{
		return solid;
	}
	
	public boolean getEditable()
	{
		return editable;
	}
	
	public int getColor()
	{
		return tileColor;
	}
	// --- End return methods ---

}
