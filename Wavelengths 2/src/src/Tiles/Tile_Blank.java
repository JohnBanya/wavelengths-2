package src.Tiles;

import src.GameData.PlayerData;
import src.Graphics.AnimatedSprite;
import src.Graphics.Screen;
import src.Graphics.Sprite;

public class Tile_Blank extends Tile
{
	/*
	Tile that acts as a solid non-color-editable tile
	*/
	protected int tileColor = 0xFFFFFFFF;
	
	protected boolean solid = false;
	protected boolean editable = false;
	
	int[] colorsToSwap = { 0xFFCC00B0 };
	
	
	public Tile_Blank(Sprite sprite)
	{
		super(sprite);
	}
	
	public Tile_Blank(Sprite sprite, int defaultColor)
	{
		super(sprite, defaultColor);
		tileColor = defaultColor;
	}
	
	public Tile_Blank(AnimatedSprite animSprite)
	{
		super(animSprite);
	}
	
	public Tile_Blank(AnimatedSprite animSprite, int defaultColor)
	{
		super(animSprite, defaultColor);
		tileColor = defaultColor;
	}
	
	public void render(Screen screen, int tile_x, int tile_y)
	{
		// Parameters of x/y are assumed to be in compressed format (tile order number, such as tile number '2' instead of x value 32)
		int x = tile_x * SIZE;
		int y = tile_y * SIZE;
		
		if(!animated)
		{
			if(tileColor == PlayerData.COLOR_TELEPORT) // if the tile is a teleport tile, render the sprite over a background tile
			{
				screen.render(x, y, Sprite.tile_blank, false, true, colorsToSwap, new int[] { 0xFFF2F2F2 }, false, new int[] { 0 });
				screen.render(x, y, sprite, false, false, new int[] { 0 }, new int[] { 0 }, true, new int[] { 0xFFCC00B0 });
			}
			else
				screen.render(x, y, sprite, false, true, colorsToSwap, new int[] { 0xFFF2F2F2 }, false, new int[] { 0 });
		}
		else
		{
			if(tileColor == PlayerData.COLOR_TELEPORT) // if the tile is a teleport tile, render the sprite over a background tile
			{
				screen.render(x, y, Sprite.tile_blank, false, true, colorsToSwap, new int[] { 0xFFF2F2F2 }, false, new int[] { 0 });
				screen.render(x, y, animSprite.getCurrentSprite(), false, false, new int[] { 0 }, new int[] { 0 }, true, new int[] { 0xFFCC00B0 });
			}
			else
				screen.render(x, y, animSprite.getCurrentSprite(), false, true, colorsToSwap, new int[] { 0xFFF2F2F2 }, false, new int[] { 0 });
		}
	}
	
	public void tick()
	{
		if(animated)
			animSprite.tick();
	}
	
	
	public void setColor(int newColor)
	{
		if(editable)
			tileColor = newColor;
	}
	
	// --- Return methods ---
	public boolean getSolid()
	{
		return solid;
	}
	
	public boolean getEditable()
	{
		return editable;
	}
	
	public int getColor()
	{
		return tileColor;
	}
	// --- End return methods ---
}
