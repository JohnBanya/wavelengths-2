package src.main;

import src.Entities.Player;
import src.GameData.PlayerData;
import src.Graphics.GameText;
import src.Graphics.Screen;
import src.Graphics.Sprite;
import src.Levels.Level;
import src.Levels.LevelLoader;
import src.Tiles.Tile;

public class GameLoader 
{
	/*
	 		Loads loader classes and handles interactions between different parts of the game
	 */
	
	private LevelLoader levelLoader;
	
	private Player player;
	private PlayerData playerData;
	
	// Key Events
	private Keyboard keyboard;
	private long lastReset; // Used to keep track of the last time the user reset
	
	// Player Death Events
	private int deathTimer = 0;

	// Console Events
	private long lastGameInfoTime = 0;
	private boolean showGameInfo = false; // Show game info in the bottom left corner
	
	public GameLoader(Keyboard key)
	{
		keyboard = key;
		lastReset = System.currentTimeMillis();
		lastGameInfoTime = System.currentTimeMillis();
		levelLoader = new LevelLoader();
		playerData = new PlayerData();
		player = new Player(levelLoader.getLevelStartX(), levelLoader.getLevelStartY(), levelLoader.getCurrentLevel(), key);
		updatePlayerColors();
		playerData.setToLowestAvailableColor();
	}
	
	public void tick()
	{
		levelLoader.tick();
		player.tick();
		tickKeyEvents();
		tickPlayerDeath();
		playerData.updateSelectedColor();
		tickTeleportation();
		
		//System.out.println("(" + player.getX() + ", " + player.getY() + ")");
	}
	
	private void tickKeyEvents()
	{
		if(keyboard.key_r && (Math.abs(lastReset - System.currentTimeMillis()) >= 500)) // If the key 'r' is pressed and the user has not reset in the last half-second, reset the level
		{
			// Reset is limited to two times per second as pressing 'r' may result in multiple resets if held down too long (lowers fps greatly)
			
			resetLevel();
			
			lastReset = System.currentTimeMillis();
		}
		
		if(keyboard.key_esc) // If the key Escape is pressed, load the menu
		{
			// Reminder: Create menu and handle key event
		}
		
		
		// Color Change key events
		// If the player hits a number and the corresponding color is unlocked, switch to that color
		if(keyboard.key_num1 && playerData.getUnlockedBlue())
			playerData.setSelectionIndex(1);
		if(keyboard.key_num2 && playerData.getUnlockedOrange())
			playerData.setSelectionIndex(2);
		if(keyboard.key_num3 && playerData.getUnlockedPurple())
			playerData.setSelectionIndex(3);
		if(keyboard.key_num4 && playerData.getUnlockedYellow())
			playerData.setSelectionIndex(4);
		if(keyboard.key_num5 && playerData.getUnlockedTeal())
			playerData.setSelectionIndex(5);
		if(keyboard.key_num6 && playerData.getUnlockedGreen())
			playerData.setSelectionIndex(6);
		
		if(keyboard.key_num0) // Toggle game info
		{
			if(Math.abs(System.currentTimeMillis() - lastGameInfoTime) >= 1000) // Allow toggle once per second
			{
				lastGameInfoTime = System.currentTimeMillis();
				
				if(showGameInfo)
					showGameInfo = false;
				else
					showGameInfo = true;
			}
		}
	}
	
	private void handleMouseEvents()
	{
		// Handles mouse events (ex. such as changing the color of tiles)
		
		// --- Button Clicks ---
		if(Mouse.getButton() == 1) // Left click
		{
			Level currentLevel = levelLoader.getCurrentLevel();
			Tile tile = currentLevel.getTile(( Mouse.getScaledX() + getPlayerX() - Game.WIDTH/2)/16, (Mouse.getScaledY() + getPlayerY() - Game.HEIGHT/2)/16); // Get the tile at the player's mouse location
			
			if(tile.getEditable() == true) // If the tile is editable, color the tile
			{
				if(tile.getColor() == 0xFFFFFFFF && (currentLevel.getTotalColored() < currentLevel.MAX_COLORED) ) // If the color of the tile is white and user has not reached the limit of colors
				{
					tile.setColor(playerData.getSelectedColor());
					currentLevel.setTotalColored(currentLevel.getTotalColored() + 1); // Add one to the total colored
				}
				else if(tile.getColor() != 0xFFFFFFFF)
				{
					tile.setColor(playerData.getSelectedColor());
				}
			}
			
		}
		else if(Mouse.getButton() == 3) // Right click
		{
			Level currentLevel = levelLoader.getCurrentLevel();
			Tile tile = currentLevel.getTile(( Mouse.getScaledX() + getPlayerX() - Game.WIDTH/2)/16, (Mouse.getScaledY() + getPlayerY() - Game.HEIGHT/2)/16); // Get the tile at the player's mouse location
			
			if(tile.getEditable() == true && tile.getColor() != 0xFFFFFFFF) // If the tile is editable and the tile is not white, reset the tile
			{
				tile.setColor(0xFFFFFFFF);
				currentLevel.setTotalColored(currentLevel.getTotalColored() - 1); // Subtract one from the total colored
			}
		}
		// --- Button Clicks ---
		
		
		// --- Mouse Wheel events ---
		
		// Change the color based on mouse wheel motion
		
		int mouseWheelMotion = Mouse.getMouseWheelMotion();
		Mouse.resetMouseWheel();
		mouseWheelMotion *= -1; // Reverse the wheel motion so that scrolling up is positive
		playerData.addToSelectionIndex(mouseWheelMotion);
		
		// --- End Mouse Wheel events ---
	}
	
	private void tickTeleportation()
	{
		// Ticks teleportation and level succession
		
		if(player.hasRequestTeleport())
		{
			levelLoader.nextLevel();
			player.setLevel(levelLoader.getCurrentLevel());
			player.setCoordinates(levelLoader.getLevelStartX(), levelLoader.getLevelStartY());
			player.reset();
			updatePlayerColors();
			
			playerData.setToLowestAvailableColor();
		}
	}
	
	private void resetLevel()
	{
		levelLoader.loadCurrentLevel();
		player.setLevel(levelLoader.getCurrentLevel());
		player.setCoordinates(levelLoader.getLevelStartX(), levelLoader.getLevelStartY());
		player.reset();
		updatePlayerColors();
	}
	
	private void updatePlayerColors()
	{
		// Updates the available colors for the given level
		
		playerData.setUnlockedBlue(levelLoader.getCurrentLevel().getLevelData().getAllowBlue());
		playerData.setUnlockedOrange(levelLoader.getCurrentLevel().getLevelData().getAllowOrange());
		playerData.setUnlockedPurple(levelLoader.getCurrentLevel().getLevelData().getAllowPurple());
		playerData.setUnlockedYellow(levelLoader.getCurrentLevel().getLevelData().getAllowYellow());
		playerData.setUnlockedGreen(levelLoader.getCurrentLevel().getLevelData().getAllowGreen());
		playerData.setUnlockedTeal(levelLoader.getCurrentLevel().getLevelData().getAllowTeal());
		
		playerData.validateIndex();
	}
	
	private void tickPlayerDeath()
	{
		// Handles events relating to player death
		
		if(player.hasBeenKilled()) // Start a timer for the time passed since player death
		{
			deathTimer++;
			
			if(deathTimer % player.DEATH_TIMER == 0) // If a certain number of ticks has passed since player death, reset the level
			{
				deathTimer = 0;
				resetLevel();
			}
		}
	}
	
	public void render(Screen screen)
	{
		handleMouseEvents(); // handleMouseEvents() is run under render so that it is run as many times as possible. This allows for better mouse detection in the method
		
		levelLoader.render(screen);
		player.render(screen);
		
		renderTotalColored(screen);
		renderColorSelection(screen);
		renderEffectBars(screen);
		renderGameInfo(screen);
	}
	
	private void renderColorSelection(Screen screen)
	{
		// Renders the GUI for color selection
		
		int xOffset = 0; // Keeps track of the total number of color gui objects rendered
		final int SHIFT_CONSTANT = 18;
		
		// X-Y positions
		final int POS_X = 20;
		final int POS_Y = 30;
		
		if(playerData.getUnlockedBlue()) // If the player has unlocked the color blue, render the appropriate gui object
		{
			if(playerData.getSelectedColor() == PlayerData.COLOR_BLUE) // If the color is currently selected
				screen.render(POS_X + xOffset*SHIFT_CONSTANT, POS_Y, Sprite.gui_colorSelection, true, true, new int[] { 0xFFFF00DC, 0xFFFF006E }, new int[] { PlayerData.COLOR_BLUE, 0xFFFFD800 }, false, new int[] { 0 });
			else // Not selected
				screen.render(POS_X + xOffset*SHIFT_CONSTANT, POS_Y, Sprite.gui_colorSelection, true, true, new int[] { 0xFFFF00DC, 0xFFFF006E }, new int[] { PlayerData.COLOR_BLUE, 0xFF808080 }, false, new int[] { 0 });
			
			xOffset++;
		}
		
		if(playerData.getUnlockedOrange()) // If the player has unlocked the color orange, render the appropriate gui object
		{
			if(playerData.getSelectedColor() == PlayerData.COLOR_ORANGE) // If the color is currently selected
				screen.render(POS_X + xOffset*SHIFT_CONSTANT, POS_Y, Sprite.gui_colorSelection, true, true, new int[] { 0xFFFF00DC, 0xFFFF006E }, new int[] { PlayerData.COLOR_ORANGE, 0xFFFFD800 }, false, new int[] { 0 });
			else // Not selected
				screen.render(POS_X + xOffset*SHIFT_CONSTANT, POS_Y, Sprite.gui_colorSelection, true, true, new int[] { 0xFFFF00DC, 0xFFFF006E }, new int[] { PlayerData.COLOR_ORANGE, 0xFF808080 }, false, new int[] { 0 });
			
			xOffset++;
		}
		
		if(playerData.getUnlockedPurple()) // If the player has unlocked the color purple, render the appropriate gui object
		{
			if(playerData.getSelectedColor() == PlayerData.COLOR_PURPLE) // If the color is currently selected
				screen.render(POS_X + xOffset*SHIFT_CONSTANT, POS_Y, Sprite.gui_colorSelection, true, true, new int[] { 0xFFFF00DC, 0xFFFF006E }, new int[] { PlayerData.COLOR_PURPLE, 0xFFFFD800 }, false, new int[] { 0 });
			else // Not selected
				screen.render(POS_X + xOffset*SHIFT_CONSTANT, POS_Y, Sprite.gui_colorSelection, true, true, new int[] { 0xFFFF00DC, 0xFFFF006E }, new int[] { PlayerData.COLOR_PURPLE, 0xFF808080 }, false, new int[] { 0 });
			
			xOffset++;
		}
		
		if(playerData.getUnlockedYellow()) // If the player has unlocked the color yellow, render the appropriate gui object
		{
			if(playerData.getSelectedColor() == PlayerData.COLOR_YELLOW) // If the color is currently selected
				screen.render(POS_X + xOffset*SHIFT_CONSTANT, POS_Y, Sprite.gui_colorSelection, true, true, new int[] { 0xFFFF00DC, 0xFFFF006E }, new int[] { PlayerData.COLOR_YELLOW, 0xFFFFD800 }, false, new int[] { 0 });
			else // Not selected
				screen.render(POS_X + xOffset*SHIFT_CONSTANT, POS_Y, Sprite.gui_colorSelection, true, true, new int[] { 0xFFFF00DC, 0xFFFF006E }, new int[] { PlayerData.COLOR_YELLOW, 0xFF808080 }, false, new int[] { 0 });
			
			xOffset++;
		}
		
		if(playerData.getUnlockedTeal()) // If the player has unlocked the color yellow, render the appropriate gui object
		{
			if(playerData.getSelectedColor() == PlayerData.COLOR_TEAL) // If the color is currently selected
				screen.render(POS_X + xOffset*SHIFT_CONSTANT, POS_Y, Sprite.gui_colorSelection, true, true, new int[] { 0xFFFF00DC, 0xFFFF006E }, new int[] { PlayerData.COLOR_TEAL, 0xFFFFD800 }, false, new int[] { 0 });
			else // Not selected
				screen.render(POS_X + xOffset*SHIFT_CONSTANT, POS_Y, Sprite.gui_colorSelection, true, true, new int[] { 0xFFFF00DC, 0xFFFF006E }, new int[] { PlayerData.COLOR_TEAL, 0xFF808080 }, false, new int[] { 0 });
			
			xOffset++;
		}
		
		if(playerData.getUnlockedGreen()) // If the player has unlocked the color green, render the appropriate gui object
		{
			if(playerData.getSelectedColor() == PlayerData.COLOR_GREEN) // If the color is currently selected
				screen.render(POS_X + xOffset*SHIFT_CONSTANT, POS_Y, Sprite.gui_colorSelection, true, true, new int[] { 0xFFFF00DC, 0xFFFF006E }, new int[] { PlayerData.COLOR_GREEN, 0xFFFFD800 }, false, new int[] { 0 });
			else // Not selected
				screen.render(POS_X + xOffset*SHIFT_CONSTANT, POS_Y, Sprite.gui_colorSelection, true, true, new int[] { 0xFFFF00DC, 0xFFFF006E }, new int[] { PlayerData.COLOR_GREEN, 0xFF808080 }, false, new int[] { 0 });
			
			xOffset++;
		}
	}
	
	private void renderTotalColored(Screen screen)
	{
		// Render the total colored / max colored text in the GUI
		
		Level currentLevel = levelLoader.getCurrentLevel();
		
		GameText displayText = new GameText(currentLevel.getTotalColored() + "/" + currentLevel.MAX_COLORED, 12, 12, 0xFFF4F4F4, 0, 0, true, false, 0);
		
		screen.render(9, 9, screen.getSpriteRectangle(displayText.getWidth() + 6, displayText.getHeight() + 6, 0xFF444444), true, false, new int[] { 0 }, new int[] { 0 }, false, new int[] { 0 });
		screen.render(10, 10, screen.getSpriteRectangle(displayText.getWidth() + 4, displayText.getHeight() + 4, 0xFF565656), true, false, new int[] { 0 }, new int[] { 0 }, false, new int[] { 0 });
		displayText.render(screen);
	}
	
	private void renderEffectBars(Screen screen)
	{
		int yOffset = 0;
		final int SHIFT_CONSTANT = 12;
		
		// X-Y positions
		final int POS_X = 20;
		final int POS_Y = 50;
		
		if(player.getGreenEffectTime() > 0) // Render green effect bar if activated
		{
			screen.render(POS_X, POS_Y + yOffset*SHIFT_CONSTANT, screen.replaceColorPercentFromLeft(Sprite.gui_effectBar, new int[] { 0xFFFF006E } , new int[] { PlayerData.COLOR_GREEN },  (double) player.getGreenTimer() / player.getGreenEffectTime() ), true, true, new int[] { 0xFFFF006E }, new int[] { 0xFFB5B5B5 }, true, new int[] { 0xFFFF00DC });
			yOffset++;
		}
		
		if(player.getOrangeTimer() > 0) // Render orange effect bar if activated
		{
			screen.render(POS_X, POS_Y + yOffset*SHIFT_CONSTANT, screen.replaceColorPercentFromLeft(Sprite.gui_effectBar, new int[] { 0xFFFF006E } , new int[] { PlayerData.COLOR_ORANGE },  (double) player.getOrangeTimer() / player.getOrangeEffectTime() ), true, true, new int[] { 0xFFFF006E }, new int[] { 0xFFB5B5B5 }, true, new int[] { 0xFFFF00DC });
			yOffset++;
		}
	}
	
	private void renderGameInfo(Screen screen)
	{
		// Renders game info in bottom left corner when activated
		
		if(showGameInfo)
		{
			new GameText("( " + ((int)player.getX()) + ", " + ((int)player.getY()) + " )", 8, Game.HEIGHT-12, 0xFFFFFFFF, 0, 0, true, true, 0).render(screen);; // X-Y coordinate 
		}
	}
	
	// --- Return methods ---
	public int getPlayerX()
	{
		return (int) player.getX();
	}
	
	public int getPlayerY()
	{
		return (int) player.getY();
	}
	// --- End Return Methods ---
}
