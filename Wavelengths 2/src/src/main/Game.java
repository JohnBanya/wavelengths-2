// Wavelengths 2.0
// 9/23/2014
package src.main;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import javax.swing.JFrame;
import src.Graphics.Screen;


public class Game extends Canvas implements Runnable
{

	private static final long serialVersionUID = 1L;
	public static String versionNumber = "2.0";
	
	// Game frame constants
	public static final int WIDTH = 500;
	public static final int HEIGHT = WIDTH / 16 * 9;
	public static final int SCALE = 3;
	public static final String TITLE = "Wavelengths";
	
	private JFrame frame; // main game frame
	private Thread thread;
	
	public boolean running;
	
	// Graphics
	private BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
	private int[] pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
	
	public double xOffset, yOffset; // Used in adjusting screen x/y
	
	// Game Objects
	private Screen screen;
	private Keyboard keyboard;
	private Mouse mouse;
	private GameLoader gameLoader;
	
	
	public Game()
	{
		// set the size of the frame
		Dimension size = new Dimension(WIDTH*SCALE, HEIGHT*SCALE);
		setPreferredSize(size);
		
		screen = new Screen(WIDTH, HEIGHT);
		
		keyboard = new Keyboard();
		addKeyListener(keyboard);
		
		mouse = new Mouse();
		addMouseListener(mouse);
		addMouseMotionListener(mouse);
		addMouseWheelListener(mouse);
		
		gameLoader = new GameLoader(keyboard);
		
		frame = new JFrame();
	}
	
	public static void main(String[] args)
	{
		Game game = new Game();
		
		game.frame.setTitle(TITLE + " v" + versionNumber);
		game.frame.setResizable(false);
		game.frame.add(game);
		game.frame.pack();
		game.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		game.frame.setLocation(175, 175);
		game.frame.setVisible(true);
		
		game.start();
	}
	
	public void start()
	{
		running = true;
		thread = new Thread(this);
		thread.start();
	}

	public void run() 
	{
		// Runs on thread start
		
		requestFocus();
		
		// Variables used to keep track of ticks per second (tps) and frames per second (fps)
		int ticksPerSecond = 0;
		int framesPerSecond = 0;
		
		// Create variables to be used in time-management (keep the game-time constant on any system)
		long startTime = System.nanoTime();
		long timer = System.currentTimeMillis();
		double changeInTime = 0;
		final double CONVERSION_FACTOR = 1000000000.0 / 60.0; // Conversion number: 1x10^9 nanoseconds (1 second) = 60 ticks
		
		while(running)
		{
			// Keep track of game time for consistency
			long currentTime = System.nanoTime();
			changeInTime = changeInTime + (currentTime - startTime) / CONVERSION_FACTOR;
			startTime = System.nanoTime();
			
			while(changeInTime > 1.0) // if the change in time is greater than '1', an in-game tick is due
			{
				tick();
				changeInTime--;
				ticksPerSecond++;
			}
			
			render();
			framesPerSecond++;
			
			// Display fps/tps
			if(System.currentTimeMillis() - timer >= 1000)
			{
				timer = System.currentTimeMillis();
				
				frame.setTitle(TITLE + " v" + versionNumber + " | " + ticksPerSecond + " tps, " + framesPerSecond + " fps");
				
				ticksPerSecond = 0;
				framesPerSecond = 0;
			}
		} // end of while(running)
	}
	
	public void tick()
	{
		// Handles time-sensitive events

		keyboard.tick();
		gameLoader.tick();
	}
	
	public void render()
	{
		// Handles graphics events
		
		BufferStrategy bs = getBufferStrategy();
		
		if(bs == null)	
		{
			createBufferStrategy(3); // Triple buffer
			return;
		}
		
		Graphics g = bs.getDrawGraphics();
	
		
		// Adjust Screen offsets
		xOffset = gameLoader.getPlayerX() - WIDTH/2;
		yOffset = gameLoader.getPlayerY() - HEIGHT/2;
		
		// reset screen offsets
		screen.setXOffset((int)xOffset);
		screen.setYOffset((int)yOffset);
		
		gameLoader.render(screen);
		
		// Clear pixel and replace with updated pixel
		int[] newPixels = screen.getPixels();
		
		for(int l = 0; l < pixels.length; l++)
		{
			pixels[l] = 0xFFFFFFFF;
			pixels[l] = newPixels[l];
		}
		
		screen.clearPixels();
		
		// Draw image to screen and release system resources
		g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
		
		g.dispose();
		bs.show();
	}
	
}